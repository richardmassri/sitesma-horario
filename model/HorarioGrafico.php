<?php
require_once ('../php/conexion.php');
class HorarioGrafico{
	public $queryDia=null;
	public $queryHora=null;
	public $queryClase=null;
	//$=null;
	public function print_horario_completo(){
		//$horario = new HorarioGrafico;
		$queryDia = $this->get_dia();
		$queryDias = $this->get_dia();
		$queryHora = $this->get_hora();
		//echo "<br> ---------------- <br>";
		//var_dump($queryHora);
		//echo $queryHora;
		//die();
		$queryClase = $this->get_query_clase();
		$queryHora = $this->get_hora();
		$resul=($this->get_rowSpanCell(1, 4, $queryHora));
		//echo "<tr><td><p>Funcion ---------------------------------: ".$resul." </p></tr></td>";
		//var_dump("------------------------------------>",$resul);
		$this->get_headTableHorarioGrafico($queryDia);
		$arrayDeudaRowSpan = $this->get_arrayDeudaDia($queryDia);
		//$arrayDeudaRowSpan[2] = 5; 
		//var_dump("------------------------------------>",$arrayDeudaRowSpan);
		//echo "<tr>";
		

		$sql =" select id, nombre from dia";
		$query = mysql_query($sql);

		$hora_mediodia=(int)strtotime('12:00:00');
		//var_dump('----------______________________-----------------<br>',$hora_mediodia, strtotime('07:30:00'));
		while($hora=mysql_fetch_assoc( $queryHora )){
			$hora_ini=(int)strtotime($hora['hora_ini']);
			if( $hora_ini > $hora_mediodia ){
				echo '
					</table>
					<table width="549" align="center" class="horario" border="2" bgcolor="#788511">
					<caption> TARDE </caption>	
				';
			}
			echo "<tr style='border-style: ridge;'>";
			echo "<td bgcolor='F0E68C'>".$hora['hora_ini'].'-'.$hora['hora_fin']."</td>";
			$queryDia = $this->queryDia;
			$sql =" select id, nombre from dia";
			$queryDia = mysql_query($sql);
			while($dia = mysql_fetch_assoc($queryDia)){
				if( $arrayDeudaRowSpan[$dia['id']] < 1 ){
					$arrayDeudaRowSpan[$dia['id']] = $this->get_print_clase($hora['id'], $dia['id'], $queryClase, $queryHora, $arrayDeudaRowSpan[$dia['id']]);
				}else{
					$arrayDeudaRowSpan[$dia['id']] -= 1;
				}
			}
			echo "</tr>";
		}

	}
	
	public function get_headTableHorarioGrafico($queryDia){
		
		echo '<tr align="center" >
					<td>Hora</td>';
		$queryDia = $this->get_dia();
		while($dia = mysql_fetch_assoc($queryDia)){
			echo '<td>'.$dia['nombre'].'</td>';
		}


	echo "</tr>";
	}

	public function get_arrayDeudaDia($queryDia){
		$arrayDia = array();
		$queryDia= $this->get_dia();
		while($dia = mysql_fetch_assoc($queryDia)){
			$arrayDia[$dia['id']]=  0;
		}
		return $arrayDia;
	}
	public function get_headTableHorarioGrafico_unificado(){
		$arrayDia = array();
		$arrayDia[]= array('hora' => 'hora');
		echo '<tr align="center" >
					<td>Hora</td>';
		$queryDia = $this->get_queryDia();
		while($dia = mysql_fetch_assoc($queryDia)){
			$arrayDia[]= array($dia['id'] => $dia['nombre']);
			echo '<td>'.$dia['nombre'].'</td>';
		}
	echo "</tr>";
		return $arrayDia;
	}
	public function get_dia(){
		require_once ('../php/conexion.php');
		$sql =" select id, nombre from dia";
		$query = mysql_query($sql);
		$this->queryDia=$query;
		return $query;
	}
	public function get_hora(){
		$sql =" select id, hora_ini, hora_fin from hora";
		$query = mysql_query($sql);
		$this->queryHora=$query;
		return $query;
	}

	public function get_turno(){
		$sql =" select id, nombre from dia";
		$query = mysql_query($sql);
		$resultado = mysql_fetch_assoc($query);
		return $resultado;
	}

	public function get_aula(){
		$sql =" select id, nombre from dia";
		$query = mysql_query($sql);
		$resultado = mysql_fetch_assoc($query);
		return $resultado;
	}

	public function get_nivelAcademico(){
		$sql =" select id, nombre from dia";
		$query = mysql_query($sql);
		$resultado = mysql_fetch_assoc($query);
		return $resultado;
	}

	public function get_nivelAcademico2(){
		$sql =" select id, nombre from dia";
		$query = mysql_query($sql);
		$resultado = mysql_fetch_assoc($query);
		return $resultado;
	}

	public function get_nivelAcademico3(){
		$sql =" select id, nombre from dia";
		$query = mysql_query($sql);
		$resultado = mysql_fetch_assoc($query);
		return $resultado;
	}

	public function get_nivelAcademico4(){
		$sql =" select id, nombre from dia";
		$query = mysql_query($sql);
		$resultado = mysql_fetch_assoc($query);
		return $resultado;
	}

	public function get_query_(){
		$sql =" select id, nombre from dia";
		$query = mysql_query($sql);
		$resultado = mysql_fetch_assoc($query);
		return $resultado;
	}

	public function get_rowSpanCell($hora_ini_id, $hora_fin_id, $queryHora){
		$span = 1;
		$hora_identificada = false;
		$forceFinish=false;
		$queryHora= $this->get_hora();
		while($hora=mysql_fetch_assoc($queryHora)){
			if($hora_identificada == true){
				if($hora['id'] <> $hora_fin_id){
					$span += 1;
				}else if($hora['id'] == $hora_fin_id){
					$forceFinish = true;
				}
			}else if($hora_identificada == false){
				if($hora['id']==$hora_ini_id){
					$hora_identificada = true;
					$span += 1;
					if($hora['id'] == $hora_fin_id){
						$forceFinish = true;
					}
				}
			}

			if($forceFinish){
				break;
			}
		} 
		return $span;
	}


	public function get_print_clase($hora_id, $dia_id, $queryClase, $queryHora, $deudaRowSpan){
		$where='';
		$join='';
		$groupBy='';
		$orderBy='';

		$rowspan=1;		

		$claseIdentificada = false;
		$forceFinish = false;
		
			$queryClase=$this->get_query_clase();
			while ($clase=mysql_fetch_assoc($queryClase)) {

				if( ($clase['hora_clase_hora_ini_id'] == $hora_id) && ($clase['clase_dia_id'] == $dia_id) ){
					//<small>
					$rowspan=$this->get_rowSpanCell( $clase['hora_clase_hora_ini_id'], $clase['hora_clase_hora_fin_id'], $queryHora );
					echo "<td bgcolor='#8FBC8F' rowspan=".$rowspan.">". $clase['materias_descripcion']."<BR> <sub style='font-size: 80%;'>Aula:".$clase['aula_cod_aula']."</sub></td>";
					$forceFinish = true;
					$deudaRowSpan=$rowspan -1 ;
					$claseIdentificada = true;
				}
					
				if($forceFinish){
					break;
				}

				# code...
			}
			//if( ($claseIdentificada == false) AND ($deudaRowSpan < 1)){
			//if( $deudaRowSpan[$dia_id] < 2 ){
				if($claseIdentificada == false AND $forceFinish <> true){
					echo "<td></td>";
				}
			//}else{
			//	$deudaRowSpan -= 1;
			//	}
		return $deudaRowSpan;
	}

	public function get_query_clase($where='',$join='',$groupBy='',$orderBy=''){
		$sql =" 
			SELECT cl.observacion AS clase_observacion, cadt.id AS cronograma_actividades_id, 
			cadt.observacion AS cronograma_actividades_observacion, secc.id AS seccion_id, 
			secc.nombre AS seccion_nombre, secc.codigo AS seccion_codigo, d.nombre AS dia_nombre, 
			t.nombre AS turno_nombre, a.nombre AS aula_nombre, a.cod_aula AS aula_cod_aula, 
			prf.nombre AS profesor_nombre, prf.apellido AS profesor_apellido, prf.cedula AS profesor_cedula, 
			mat.cod_mat AS materias_cod_mat, mat.descripcion AS materias_descripcion, 
			hclasi.hora_ini AS hora_hora_ini, hclasf.hora_fin AS hora_fin, 

			cl.dia_id AS clase_dia_id,
			hc.hora_ini_id AS hora_clase_hora_ini_id,
			hc.hora_fin_id AS hora_clase_hora_fin_id

			FROM 
			clase cl
			LEFT JOIN dia AS d ON d.id = cl.dia_id
			LEFT JOIN turno AS t ON t.id = cl.turno_id
			LEFT JOIN aula AS a ON a.id = cl.aula_id
			LEFT JOIN profesor AS prf ON prf.id_profesor = cl.profesor_id
			LEFT JOIN hora_clase AS hc ON hc.id = cl.hora_clase_id
			LEFT JOIN materias AS mat ON mat.id = cl.materias_id
			LEFT JOIN cronograma_actividades AS cadt ON cadt.id = cl.cronograma_actividades_id
			LEFT JOIN seccion AS secc ON secc.id = cadt.seccion_id
			LEFT JOIN hora AS hclasi ON hclasi.id = hc.hora_ini_id
			LEFT JOIN hora AS hclasf ON hclasf.id = hc.hora_fin_id
			 $join

			WHERE 1=1 $where
			$groupBy
			$orderBy
		";
		$query = mysql_query($sql);
		//$resultado = mysql_fetch_assoc($query);
		$this->queryClase=$query;
		return $query;
	}

}


?>