function Verificar() {
	var filter=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
	var num = /^([0-9])*$/ ;
	
	if($('#nombre').val() == "") {
		alert("Por favor indique Nombre Completo");
		$('#nombre').focus();
		return false;
	} 

	if($('#apellido').val() == "") {
		alert("Por favor indique Apellido Completo");
		$('#apellido').focus();
		return false;
	} 

	if($('#ci').val() == "") {
		alert("Por favor indique C.I.");
		$('#ci').focus();
		return false;
	}

	if(!num.test($('#ci').val())) {
		alert("Por favor indique Numero C.I.");
		$('#ci').focus();
		return false;
	}

	if($('#sexo').val() == "") {
		alert("Por favor indique Sexo");
		$('#sexo').focus();
		return false;
	} 

	if($('#fecha_nac').val() == "") {
		alert("Por favor indique Fecha de Nacimiento");
		$('#fecha_nac').focus();
		return false;
	} 

	if($('#edad').val() == "") {
		alert("Por favor indique Edad");
		$('#edad').focus();
		return false;
	}

	if($('#est').val() == "") {
		alert("Por favor indique Estatura");
		$('#est').focus();
		return false;
	} 

	if($('#peso').val() == "") {
		alert("Por favor indique Peso");
		$('#peso').focus();
		return false;
	} 

	if($('#lugar_naci').val() == "") {
		alert("Por favor indique Lugar de Nacimiento");
		$('#lugar_naci').focus();
		return false;
	} 

	if($('#correo').val() == "") {
		alert("Por favor indique Correo");
		$('#correo').focus();
		return false;
	} 
	
	if($('#ano_curs').val() == "") {
		alert("Por favor indique a\u00f1o  a Cursar");
		$('#ano_curs').focus();
		return false;
	} 

	if($('#per').val() == "") {
		alert("Por favor indique Periodo");
		$('#per').focus();
		return false;
	} 

	if($('#telf').val() == "") {
		alert("Por favor indique Telefono");
		$('#telf').focus();
		return false;
	}

	if($('#materias_cursada').length>0)
		if($('#materias_cursada').val() == "") {
			alert("Por favor indique Cantidad de Materias a Cursar");
			$('#materias_cursada').focus();
			return false;
		} 

	if($('#direccion').val() == "") {
		alert("Por favor indique Direccion");
		$('#direccion').focus();
		return false;
	}
	
	/*if(!filter.test($('#email3').val())) {
		alert("Por favor indique correo electronico");
		$('#email3').focus();
		return false;
	}*/
	return true;
}

function Verificar_representante() {
	var num = /^([0-9])*$/ ; 
	if($('#nombre_r').val() == "") {
		alert("Por favor indique Nombre Completo");
		$('#nombre_r').focus();
		return false;
	}

	if($('#apellido_r').val() == "") {
		alert("Por favor indique Apellido Completo");
		$('#apellido_r').focus();
		return false;
	} 

	if($('#ci_r').val() == "") {
		alert("Por favor indique C.I.");
		$('#ci_r').focus();
		return false;
	}
	
	//////SEXO
	if(!num.test($('#ci_r').val())) {
		alert("Por favor indique correcto C.I.");
		$('#ci_r').focus();
		return false;
	}

	/*
	if($('#sexo_r.checked != "cheked" && $('#sexo_r').val() == "F" ) {
		alert("Por favor indique Sexo");
		$('#ci_r').focus();
		return false;
	} 
	*/

	if($('#fecha_nac_r').val() == "") {
		alert("Por favor indique Fecha de Nacimiento");
		$('#fecha_nac_r').focus();
		return false;
	} 

	if($('#edad_r').val() == "") {
		alert("Por favor indique Edad");
		$('#edad_r').focus();
		return false;
	}

	if($('#trabajo_r').val() == "") {
		alert("Por favor indique Nombre de la Empresa");
		$('#trabajo_r').focus();
		return false;
	}

	if($('#lugar_trabajo_r').val() == "") {
		alert("Por favor indique Lugar de Trabajo");
		$('#lugar_trabajo_r').focus();
		return false;
	} 
	
	if($('#telf_r').val() == "") {
		alert("Por favor indique Telefono");
		$('#telf_r').focus();
		return false;
	}

	if($('#correo_r').val() == "") {
		alert("Por favor indique Correo");
		$('#correo_r').focus();
		return false;
	} 

	if($('#parentesco_r').val() == "") {
		alert("Por favor indique Parentesco");
		$('#parentesco_r').focus();
		return false;
	}

	return true;
}

function Verificar_usuario() {
	var num = /^([0-9])*$/ ;
	
	if($('#nombre').val() == "") {
		alert("Por favor indique Nombre");
		$('#nombre').focus();
		return false;
	}

	if($('#apellido').val() == "") {
		alert("Por favor indique apellido");
		$('#apellido').focus();
		return false;
	} 

	if($('#ci').val() == "") {
		alert("Por favor indique ci");
		$('#ci').focus();
		return false;
	}

	if(!num.test($('#ci').val())) {
		alert("Por favor indique correcto ci");
		$('#ci').focus();
		return false;
	}

	if($('#usuario').val() == "") {
		alert("Por favor indique usuario");
		$('#usuario').focus();
		return false;
	}

	if($('#clave').val() == "") {
		alert("Por favor indique clave");
		$('#clave').focus();
		return false;
	}

	if($('#perfil').val() == "") {
		alert("Por favor indique perfil");
		$('#perfil').focus();
		return false;
	}

	return true;
}