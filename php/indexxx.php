﻿<?php 
	session_start();
	include('../php/functiones.php'); 
	
	if(isset($_SESSION['user']) and isset($_SESSION['clave'])){
		$devuelta = validacion_sesion($_SESSION['user'],$_SESSION['clave']);
		echo $devuelta;
	}else{
		header('Location: ../');
		//echo "<script>alert('Acceso Negado no'); location = '../';</script>";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

          <script type="text/javascript" src="../js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="../js/jquery.lksMenu.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/lksMenuSkin3.css" />
        <script>
        $('document').ready(function(){
            $('.menu').lksMenu();
        });
        </script>
  

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>..:: Sistema de Inscripcion ::..</title>
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
		<script language="javascript" type="text/javascript">
		<!--
			function cambio_img(img,id) {
				myUrl = '../img/'+img;
				document.getElementById(id).src = myUrl;
			}
		//-->
		</script>
	</head>
	<body>
		<table width="980" border="0"  align="center" cellpadding="1" cellspacing="1">
			<tr>
				<td colspan="4" align="center">
					<img src="../img/baneer_institucional.png" align="center" width="980px" /><br>
					<img src="../img/baneer_principal.png" height="140px" align="center" style="margin-bottom: -8px;" /><br>
					<hr style="border-collapse: collapse; border: 0px none; background-color: #0B3A1B; width: 980px;" size="3" />
				</td>
			</tr>
			<tr>
				<td id="contenedor" style="padding: 0 !important;">
					<table width="980" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; margin-top: -8px !important;">
						<tr>
							<td id="menu">



  <div class="menu">
        <div>
                 </div>

            <br/>
        <ul>
            <li>
               		 <a href="#"><img src="../img/registrar_alumno_1.png"
										onmouseover="cambio_img('registrar_alumno_2.png','boton1')"
										 onmouseout="cambio_img('registrar_alumno_1.png','boton1')" id="boton1" /></a>
									
								
                <ul>
                    <li><a href="registro.php" target="paginas">Estudiante</a></li>
                    <li><a href="profesor.php" target="paginas">Profesor</a></li
		<li><a href="horario.php" target="paginas">horario</a></li>
               
                 
                </ul>
            </li>
            <li>
               <?php if($_SESSION['perfil'] == "1"){?>
								<a >
									<img id="boton2" src="../img/busqueda_1.png"
										onmouseover="cambio_img('busqueda_2.png','boton2')" 
										onmouseout="cambio_img('busqueda_1.png','boton2')" />
								</a>
                <ul>
                    <li><a target="paginas" href="busqueda.php">Reportes</a></li>
                  
                </ul>
            </li>
            <li>
               
								<?php } ?>
								<a href="formatos.php" target="paginas">
									<img src="../img/reporte_1.png" 
										onmouseover="cambio_img('reporte_2.png','boton3')" 
										onmouseout="cambio_img('reporte_1.png','boton3')" id="boton3" />
								</a>
								
                <ul>
                    <li><a href="http://www.tutorialjquery.com">Tutorial jQuery</a></li>
                    <li><a href="http://www.jsonpexamples.com">jsonp Examples</a></li>
                    <li><a href="http://www.codigogratis.com.ar">Codigo Gratis</a></li>
                    <li><a href="http://www.bugbase.com.ar">Bug Base</a></li>
                </ul>
            </li>
            <li>
               	
								<?php if($_SESSION['perfil'] == "1"){?>
								<a>
									<img src="../img/gestor_usuarios_1.png"
										onmouseover="cambio_img('gestor_usuarios_2.png','botonUsuario')"
										onmouseout="cambio_img('gestor_usuarios_1.png','botonUsuario')"  id="botonUsuario"/>
								</a>
								<?php } ?>
                <ul>
                    <li><a href="registro_usuario.php" target="paginas">Registar usuarios</a></li>
                   
                </ul>
            </li>
        </ul>
    </div>



						
							
							</td>
							<td>
								<div align="right" style="font-family: sans-serif; font-size: 10px; font-weight: bold; color: #0B3A1B;">
									<?php echo "Bienvenido(a) ".nombre_usuario($_SESSION['user']); ?>
									&nbsp;|&nbsp;
									<a href="salir.php" style="text-decoration:none; font-family: sans-serif; font-size: 10px; font-weight: bold; color: #155C2E;" onclick="javascript: return( confirm('¿Seguro que desea cerrar sesión?') );">
										<img src="../img/cerrar.png" id="boton_cerrar" border="0" />
									
									</a>
								</div>
								<iframe class="paginas" name="paginas" frameborder="0"></iframe>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="4" colspan="4" bgcolor="#0B3A1B"></td>
			</tr>


		</table>



	</body>

</html>
