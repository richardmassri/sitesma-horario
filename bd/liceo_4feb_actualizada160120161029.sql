-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 16-01-2016 a las 10:41:39
-- Versión del servidor: 5.5.44-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `liceo_4feb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE IF NOT EXISTS `alumnos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `cedula` int(11) NOT NULL,
  `telf` int(12) NOT NULL,
  `direccion` text NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `edad` int(11) NOT NULL,
  `fecha_nac` date NOT NULL,
  `estatura` varchar(5) NOT NULL,
  `peso` int(5) NOT NULL,
  `lugar_nac` text NOT NULL,
  `correo` varchar(80) NOT NULL,
  `ano_curso` int(2) NOT NULL,
  `periodo` varchar(20) NOT NULL,
  `estatus` varchar(8) NOT NULL,
  `seccion` varchar(4) NOT NULL,
  `id_representante` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anio_escolar`
--

CREATE TABLE IF NOT EXISTS `anio_escolar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anio` year(4) NOT NULL,
  `observacion` text,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ano1`
--

CREATE TABLE IF NOT EXISTS `ano1` (
  `id_ano1` int(5) NOT NULL AUTO_INCREMENT,
  `cedula` int(10) NOT NULL,
  `cl` int(5) NOT NULL,
  `in` int(5) NOT NULL,
  `ma` int(5) NOT NULL,
  `en` int(5) NOT NULL,
  `hv` int(5) NOT NULL,
  `ef` int(5) NOT NULL,
  `gg` int(5) NOT NULL,
  `ea` int(5) NOT NULL,
  `ed` int(5) NOT NULL,
  `et` int(5) NOT NULL,
  `cl2` int(5) NOT NULL,
  `in2` int(5) NOT NULL,
  `ma2` int(5) NOT NULL,
  `en2` int(5) NOT NULL,
  `hv2` int(5) NOT NULL,
  `ef2` int(5) NOT NULL,
  `gg2` int(5) NOT NULL,
  `ea2` int(5) NOT NULL,
  `ed2` int(5) NOT NULL,
  `et2` int(5) NOT NULL,
  `cl3` int(5) NOT NULL,
  `in3` int(5) NOT NULL,
  `ma3` int(5) NOT NULL,
  `en3` int(5) NOT NULL,
  `hv3` int(5) NOT NULL,
  `ef3` int(5) NOT NULL,
  `gg3` int(5) NOT NULL,
  `ea3` int(5) NOT NULL,
  `ed3` int(5) NOT NULL,
  `et3` int(5) NOT NULL,
  PRIMARY KEY (`id_ano1`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ano2`
--

CREATE TABLE IF NOT EXISTS `ano2` (
  `cl` int(5) DEFAULT NULL,
  `in` int(5) DEFAULT NULL,
  `ma` int(5) DEFAULT NULL,
  `es` int(5) DEFAULT NULL,
  `cb` int(5) DEFAULT NULL,
  `hv` int(5) DEFAULT NULL,
  `hu` int(5) DEFAULT NULL,
  `ea` int(5) DEFAULT NULL,
  `ef` int(5) DEFAULT NULL,
  `et` int(5) DEFAULT NULL,
  `cl2` int(5) DEFAULT NULL,
  `in2` int(5) DEFAULT NULL,
  `ma2` int(5) DEFAULT NULL,
  `es2` int(5) DEFAULT NULL,
  `cb2` int(5) DEFAULT NULL,
  `hv2` int(5) DEFAULT NULL,
  `hu2` int(5) DEFAULT NULL,
  `ea2` int(5) DEFAULT NULL,
  `ef2` int(5) DEFAULT NULL,
  `et2` int(5) DEFAULT NULL,
  `cl3` int(5) DEFAULT NULL,
  `in3` int(5) DEFAULT NULL,
  `ma3` int(5) DEFAULT NULL,
  `es3` int(5) DEFAULT NULL,
  `cb3` int(5) DEFAULT NULL,
  `hv3` int(5) DEFAULT NULL,
  `hu3` int(5) DEFAULT NULL,
  `ea3` int(5) DEFAULT NULL,
  `ef3` int(5) DEFAULT NULL,
  `et3` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ano3`
--

CREATE TABLE IF NOT EXISTS `ano3` (
  `cl` int(5) DEFAULT NULL,
  `in` int(5) DEFAULT NULL,
  `ma` int(5) DEFAULT NULL,
  `cb` int(5) DEFAULT NULL,
  `fi` int(5) DEFAULT NULL,
  `qu` int(5) DEFAULT NULL,
  `hv` int(5) DEFAULT NULL,
  `gv` int(5) DEFAULT NULL,
  `ef` int(5) DEFAULT NULL,
  `et` int(5) DEFAULT NULL,
  `cl2` int(5) DEFAULT NULL,
  `in2` int(5) DEFAULT NULL,
  `ma2` int(5) DEFAULT NULL,
  `cb2` int(5) DEFAULT NULL,
  `fi2` int(5) DEFAULT NULL,
  `qu2` int(5) DEFAULT NULL,
  `hv2` int(5) DEFAULT NULL,
  `gv2` int(5) DEFAULT NULL,
  `ef2` int(5) DEFAULT NULL,
  `et2` int(5) DEFAULT NULL,
  `cl3` int(5) DEFAULT NULL,
  `in3` int(5) DEFAULT NULL,
  `ma3` int(5) DEFAULT NULL,
  `cb3` int(5) DEFAULT NULL,
  `fi3` int(5) DEFAULT NULL,
  `qu3` int(5) DEFAULT NULL,
  `hv3` int(5) DEFAULT NULL,
  `gv3` int(5) DEFAULT NULL,
  `ef3` int(5) DEFAULT NULL,
  `et3` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ano4`
--

CREATE TABLE IF NOT EXISTS `ano4` (
  `cl` int(5) DEFAULT NULL,
  `ma` int(5) DEFAULT NULL,
  `hc` int(5) DEFAULT NULL,
  `in` int(5) DEFAULT NULL,
  `ef` int(5) DEFAULT NULL,
  `fi` int(5) DEFAULT NULL,
  `qu` int(5) DEFAULT NULL,
  `cb` int(5) DEFAULT NULL,
  `dt` int(5) DEFAULT NULL,
  `fl` int(5) DEFAULT NULL,
  `ip` int(5) DEFAULT NULL,
  `cl2` int(5) DEFAULT NULL,
  `ma2` int(5) DEFAULT NULL,
  `hc2` int(5) DEFAULT NULL,
  `in2` int(5) DEFAULT NULL,
  `ef2` int(5) DEFAULT NULL,
  `fi2` int(5) DEFAULT NULL,
  `qu2` int(5) DEFAULT NULL,
  `cb2` int(5) DEFAULT NULL,
  `dt2` int(5) DEFAULT NULL,
  `fl2` int(5) DEFAULT NULL,
  `ip2` int(5) DEFAULT NULL,
  `cl3` int(5) DEFAULT NULL,
  `ma3` int(5) DEFAULT NULL,
  `hc3` int(5) DEFAULT NULL,
  `in3` int(5) DEFAULT NULL,
  `ef3` int(5) DEFAULT NULL,
  `fi3` int(5) DEFAULT NULL,
  `qu3` int(5) DEFAULT NULL,
  `cb3` int(5) DEFAULT NULL,
  `dt3` int(5) DEFAULT NULL,
  `fl3` int(5) DEFAULT NULL,
  `ip3` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ano5`
--

CREATE TABLE IF NOT EXISTS `ano5` (
  `in` int(5) DEFAULT NULL,
  `ef` int(5) DEFAULT NULL,
  `ge` int(5) DEFAULT NULL,
  `cl` int(5) DEFAULT NULL,
  `ma` int(5) DEFAULT NULL,
  `fi` int(5) DEFAULT NULL,
  `qu` int(5) DEFAULT NULL,
  `cb` int(5) DEFAULT NULL,
  `ct` int(5) DEFAULT NULL,
  `ip` int(5) DEFAULT NULL,
  `in2` int(5) DEFAULT NULL,
  `ef2` int(5) DEFAULT NULL,
  `ge2` int(5) DEFAULT NULL,
  `cl2` int(5) DEFAULT NULL,
  `ma2` int(5) DEFAULT NULL,
  `fi2` int(5) DEFAULT NULL,
  `qu2` int(5) DEFAULT NULL,
  `cb2` int(5) DEFAULT NULL,
  `ct2` int(5) DEFAULT NULL,
  `ip2` int(5) DEFAULT NULL,
  `in3` int(5) DEFAULT NULL,
  `ef3` int(5) DEFAULT NULL,
  `ge3` int(5) DEFAULT NULL,
  `cl3` int(5) DEFAULT NULL,
  `ma3` int(5) DEFAULT NULL,
  `fi3` int(5) DEFAULT NULL,
  `qu3` int(5) DEFAULT NULL,
  `cb3` int(5) DEFAULT NULL,
  `ct3` int(5) DEFAULT NULL,
  `ip3` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprobacion`
--

CREATE TABLE IF NOT EXISTS `aprobacion` (
  `grado` int(1) NOT NULL,
  `condicion` varchar(4) NOT NULL,
  `id_aprob` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_aprob`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `aprobacion`
--

INSERT INTO `aprobacion` (`grado`, `condicion`, `id_aprob`) VALUES
(1, '12', 1),
(2, '12', 2),
(3, '12', 3),
(4, '14', 4),
(5, '7', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacion_nota`
--

CREATE TABLE IF NOT EXISTS `asignacion_nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `estatus_asignacion` varchar(1) NOT NULL DEFAULT 'C',
  `observacion` text,
  `lapso_id` int(11) NOT NULL,
  `materias_id` int(11) NOT NULL,
  `profesor_id` int(11) NOT NULL,
  `inscripcion_id` int(11) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_asignacion_nota_lapso_lapso_id` (`lapso_id`),
  KEY `fk_asignacion_nota_materias_materias_id` (`materias_id`),
  KEY `fk_asignacion_nota_profesor_profesor_id` (`profesor_id`),
  KEY `fk_asignacion_nota_inscripcion_inscripcion_id` (`inscripcion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aula`
--

CREATE TABLE IF NOT EXISTS `aula` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `cod_aula` varchar(30) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clase`
--

CREATE TABLE IF NOT EXISTS `clase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `observacion` text,
  `aula_id` int(11) NOT NULL,
  `profesor_id` int(11) NOT NULL,
  `hora_clase_id` int(11) NOT NULL,
  `materias_id` int(11) NOT NULL,
  `cronograma_actividades_id` int(11) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_clase_aula_aula_id` (`aula_id`),
  KEY `fk_clase_profesor_profesor_id` (`profesor_id`),
  KEY `fk_clase_hora_clase_hora_clase_id` (`hora_clase_id`),
  KEY `fk_clase_materias_materias_id` (`materias_id`),
  KEY `fk_clase_cronograma_actividades_cronograma_actividades_id` (`cronograma_actividades_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cronograma_actividades`
--

CREATE TABLE IF NOT EXISTS `cronograma_actividades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seccion_id` int(11) NOT NULL,
  `plan_estudio_id` int(11) NOT NULL,
  `observacion` text,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_cronograma_actividades_seccion_seccion_id` (`seccion_id`),
  KEY `fk_cronograma_actividades_plan_estudio_plan_estudio_id` (`plan_estudio_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_alumnos`
--

CREATE TABLE IF NOT EXISTS `datos_alumnos` (
  `id_alumno` int(11) NOT NULL,
  `periodo` varchar(10) NOT NULL,
  `materias_cursadas` int(3) NOT NULL,
  `materias_aprobadas` int(4) NOT NULL,
  `aprobacion` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_alumno`,`periodo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hora`
--

CREATE TABLE IF NOT EXISTS `hora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hora_ini` time NOT NULL,
  `hora_fin` time NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hora_clase`
--

CREATE TABLE IF NOT EXISTS `hora_clase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hora_ini_id` int(11) NOT NULL,
  `hora_fin_id` int(11) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_hora_clase_hora_hora_ini_id` (`hora_ini_id`),
  KEY `fk_hora_clase_hora_hora_fin_id` (`hora_fin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripcion`
--

CREATE TABLE IF NOT EXISTS `inscripcion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `estatus_inscripcion` varchar(1) NOT NULL DEFAULT 'I',
  `observacion` text,
  `alumnos_id` int(11) NOT NULL,
  `representante_id` int(11) NOT NULL,
  `cronograma_actividades_id` int(11) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_inscripcion_alumnos_alumnos_id` (`alumnos_id`),
  KEY `fk_inscripcion_representante_representante_id` (`representante_id`),
  KEY `fk_inscripcion_cronograma_actividades_cronograma_actividades_id` (`cronograma_actividades_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lapso`
--

CREATE TABLE IF NOT EXISTS `lapso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logins`
--

CREATE TABLE IF NOT EXISTS `logins` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(35) NOT NULL,
  `estatus` int(1) NOT NULL,
  `perfil` int(1) NOT NULL,
  `id_personas` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Volcado de datos para la tabla `logins`
--

INSERT INTO `logins` (`id_usuario`, `usuario`, `clave`, `estatus`, `perfil`, `id_personas`) VALUES
(25, 'hbravo', 'e2db52903d3ecc800cd5d719aee01d98', 1, 1, 36),
(26, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 1, 37);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

CREATE TABLE IF NOT EXISTS `materias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_mat` int(2) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`id`, `cod_mat`, `descripcion`) VALUES
(1, 1, 'Matematicas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias_plan_estudio`
--

CREATE TABLE IF NOT EXISTS `materias_plan_estudio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estatus_materias` varchar(1) NOT NULL DEFAULT 'C',
  `materias_id` int(11) NOT NULL,
  `plan_estudio_id` int(11) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_materias_plan_estudio_materias_materias_id` (`materias_id`),
  KEY `fk_materias_plan_estudio_plan_estudio_plan_estudio_id` (`plan_estudio_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel_academico`
--

CREATE TABLE IF NOT EXISTS `nivel_academico` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `cod_anio` int(2) NOT NULL,
  `descripcion` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `nivel_academico`
--

INSERT INTO `nivel_academico` (`id`, `cod_anio`, `descripcion`) VALUES
(1, 1, 'Primer Año'),
(2, 2, 'Segundo Año');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodo`
--

CREATE TABLE IF NOT EXISTS `periodo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `descripcion` text,
  `anio_escolar_ini_id` int(11) NOT NULL,
  `anio_escolar_fin_id` int(11) NOT NULL,
  `fecha_inicio_periodo` datetime NOT NULL,
  `fecha_fin_periodo` datetime NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_periodo_anio_escolar_anio_escolar_ini_id` (`anio_escolar_ini_id`),
  KEY `fk_periodo_anio_escolar_anio_escolar_fin_id` (`anio_escolar_fin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE IF NOT EXISTS `personas` (
  `id_personas` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `apellido` varchar(20) NOT NULL,
  `ci` int(10) NOT NULL,
  PRIMARY KEY (`id_personas`),
  UNIQUE KEY `ci` (`ci`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id_personas`, `nombre`, `apellido`, `ci`) VALUES
(36, 'Hernaldo', 'Davila', 8029211),
(37, 'Administrador', 'Administrador', 123456);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_estudio`
--

CREATE TABLE IF NOT EXISTS `plan_estudio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `nivel_academico_id` int(11) NOT NULL,
  `periodo_id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `especial_plan_estudio` varchar(1) NOT NULL DEFAULT 'N',
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_plan_estudio_nivel_academico_nivel_academico_id` (`nivel_academico_id`),
  KEY `fk_plan_estudio_periodo_periodo_id` (`periodo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE IF NOT EXISTS `profesor` (
  `id_profesor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `cedula` int(11) NOT NULL,
  `telefono` int(12) NOT NULL,
  `correo` varchar(80) NOT NULL,
  `materia1` varchar(50) NOT NULL,
  `dh` varchar(50) NOT NULL,
  `materia2` varchar(50) NOT NULL,
  `dh1` varchar(50) NOT NULL,
  PRIMARY KEY (`id_profesor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor_materia`
--

CREATE TABLE IF NOT EXISTS `profesor_materia` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `cedula_prof` int(11) NOT NULL,
  `cod_materia` int(3) NOT NULL,
  `cod_anio` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cedula_prof` (`cedula_prof`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `representante`
--

CREATE TABLE IF NOT EXISTS `representante` (
  `id_r` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_r` varchar(80) NOT NULL,
  `apellido_r` varchar(80) NOT NULL,
  `cedula_r` int(11) NOT NULL,
  `edad_r` int(2) NOT NULL,
  `fecha_nac_r` date NOT NULL,
  `telefono_r` int(12) NOT NULL,
  `trabajo_r` varchar(80) NOT NULL,
  `direccion_trabajo_r` text NOT NULL,
  `correo_r` varchar(80) NOT NULL,
  `parentesco` varchar(20) NOT NULL,
  `sexo_r` char(1) NOT NULL DEFAULT 'F',
  PRIMARY KEY (`id_r`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion`
--

CREATE TABLE IF NOT EXISTS `seccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `asignacion_nota`
--
ALTER TABLE `asignacion_nota`
  ADD CONSTRAINT `fk_asignacion_nota_lapso_lapso_id` FOREIGN KEY (`lapso_id`) REFERENCES `lapso` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_asignacion_nota_materias_materias_id` FOREIGN KEY (`materias_id`) REFERENCES `materias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_asignacion_nota_profesor_profesor_id` FOREIGN KEY (`profesor_id`) REFERENCES `profesor` (`id_profesor`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_asignacion_nota_inscripcion_inscripcion_id` FOREIGN KEY (`inscripcion_id`) REFERENCES `inscripcion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `clase`
--
ALTER TABLE `clase`
  ADD CONSTRAINT `fk_clase_aula_aula_id` FOREIGN KEY (`aula_id`) REFERENCES `aula` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_clase_profesor_profesor_id` FOREIGN KEY (`profesor_id`) REFERENCES `profesor` (`id_profesor`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_clase_hora_clase_hora_clase_id` FOREIGN KEY (`hora_clase_id`) REFERENCES `hora_clase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_clase_materias_materias_id` FOREIGN KEY (`materias_id`) REFERENCES `materias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_clase_cronograma_actividades_cronograma_actividades_id` FOREIGN KEY (`cronograma_actividades_id`) REFERENCES `cronograma_actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cronograma_actividades`
--
ALTER TABLE `cronograma_actividades`
  ADD CONSTRAINT `fk_cronograma_actividades_seccion_seccion_id` FOREIGN KEY (`seccion_id`) REFERENCES `seccion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cronograma_actividades_plan_estudio_plan_estudio_id` FOREIGN KEY (`plan_estudio_id`) REFERENCES `plan_estudio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `hora_clase`
--
ALTER TABLE `hora_clase`
  ADD CONSTRAINT `fk_hora_clase_hora_hora_ini_id` FOREIGN KEY (`hora_ini_id`) REFERENCES `hora` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hora_clase_hora_hora_fin_id` FOREIGN KEY (`hora_fin_id`) REFERENCES `hora` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD CONSTRAINT `fk_inscripcion_alumnos_alumnos_id` FOREIGN KEY (`alumnos_id`) REFERENCES `alumnos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_inscripcion_representante_representante_id` FOREIGN KEY (`representante_id`) REFERENCES `representante` (`id_r`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_inscripcion_cronograma_actividades_cronograma_actividades_id` FOREIGN KEY (`cronograma_actividades_id`) REFERENCES `cronograma_actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `materias_plan_estudio`
--
ALTER TABLE `materias_plan_estudio`
  ADD CONSTRAINT `fk_materias_plan_estudio_materias_materias_id` FOREIGN KEY (`materias_id`) REFERENCES `materias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_materias_plan_estudio_plan_estudio_plan_estudio_id` FOREIGN KEY (`plan_estudio_id`) REFERENCES `plan_estudio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `periodo`
--
ALTER TABLE `periodo`
  ADD CONSTRAINT `fk_periodo_anio_escolar_anio_escolar_ini_id` FOREIGN KEY (`anio_escolar_ini_id`) REFERENCES `anio_escolar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_periodo_anio_escolar_anio_escolar_fin_id` FOREIGN KEY (`anio_escolar_fin_id`) REFERENCES `anio_escolar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `plan_estudio`
--
ALTER TABLE `plan_estudio`
  ADD CONSTRAINT `fk_plan_estudio_nivel_academico_nivel_academico_id` FOREIGN KEY (`nivel_academico_id`) REFERENCES `nivel_academico` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_plan_estudio_periodo_periodo_id` FOREIGN KEY (`periodo_id`) REFERENCES `periodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
