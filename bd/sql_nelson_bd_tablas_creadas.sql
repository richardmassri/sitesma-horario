--drop table hora;
CREATE TABLE hora (
id int not null AUTO_INCREMENT PRIMARY KEY,
hora_ini TIME not null ,
hora_fin TIME not null ,

usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'
)


--drop table anio_escolar;

--------------------------------------------------------------------------------------------------->
--drop table dia ;
CREATE TABLE dia (
id int not null AUTO_INCREMENT PRIMARY KEY,
nombre character varying(9) not null,

usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'
)

--drop table turno ;
CREATE TABLE turno (
id int not null AUTO_INCREMENT PRIMARY KEY,
nombre character varying(9) not null,

usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'
)
--------------------------------------------------------------------------------------------------->

>>>>>>> horario_grafico
CREATE TABLE anio_escolar(
id int not null AUTO_INCREMENT PRIMARY KEY,
anio YEAR not null,
observacion text,
usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'
)
--drop table seccion;
CREATE TABLE seccion(
id int not null AUTO_INCREMENT PRIMARY KEY,
nombre character varying(30) not null,

usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'
)

--drop table aula;
CREATE TABLE aula(
id int not null AUTO_INCREMENT PRIMARY KEY,
nombre character varying(80) not null,
cod_aula  character varying(30) not null,
usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'
)


--drop table periodo;
CREATE TABLE periodo(
id int not null AUTO_INCREMENT PRIMARY KEY,
nombre character varying(30) not null,
descripcion text, 
anio_escolar_ini_id int not null,
anio_escolar_fin_id int not null,
fecha_inicio_periodo DateTime not null,
fecha_fin_periodo DateTime not null,
usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'

, CONSTRAINT fk_periodo_anio_escolar_anio_escolar_ini_id 
	FOREIGN KEY (anio_escolar_ini_id) REFERENCES anio_escolar(id)
	ON DELETE CASCADE ON UPDATE CASCADE
, CONSTRAINT fk_periodo_anio_escolar_anio_escolar_fin_id 
	FOREIGN KEY (anio_escolar_fin_id) REFERENCES anio_escolar(id)
	ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;


--drop table hora_clase;
CREATE TABLE hora_clase(
id int not null AUTO_INCREMENT PRIMARY KEY,
hora_ini_id int not null,
hora_fin_id int not null,
usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'

, CONSTRAINT fk_hora_clase_hora_hora_ini_id 
	FOREIGN KEY (hora_ini_id) REFERENCES hora(id)
	ON DELETE CASCADE ON UPDATE CASCADE
, CONSTRAINT fk_hora_clase_hora_hora_fin_id
	FOREIGN KEY (hora_fin_id) REFERENCES hora(id)
	ON DELETE CASCADE ON UPDATE CASCADE


	
) ENGINE=InnoDB;


/*--drop table plan_estudio;
CREATE TABLE plan_estudio(
id int not null AUTO_INCREMENT PRIMARY KEY,
nombre character varying(80) not null,
nivel_academico_id int not null,
descripcion text not null,


usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'

, CONSTRAINT fk_plan_estudio_nivel_academico_nivel_academico_id 
	FOREIGN KEY (nivel_academico_id) REFERENCES nivel_academico(id)
	ON DELETE CASCADE ON UPDATE CASCADE


	
) ENGINE=InnoDB;
*/

--drop table plan_estudio;
CREATE TABLE plan_estudio(
id int not null AUTO_INCREMENT PRIMARY KEY,
nombre character varying(80) not null,
nivel_academico_id int not null,
periodo_id int not null,
descripcion text not null,
especial_plan_estudio character varying(1) NOT NULL DEFAULT 'N', -- estatus: N = 'no', S = 'si'

usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'

, CONSTRAINT fk_plan_estudio_nivel_academico_nivel_academico_id 
	FOREIGN KEY (nivel_academico_id) REFERENCES nivel_academico(id)
	ON DELETE CASCADE ON UPDATE CASCADE
, CONSTRAINT fk_plan_estudio_periodo_periodo_id 
	FOREIGN KEY (periodo_id) REFERENCES periodo(id)
	ON DELETE CASCADE ON UPDATE CASCADE


	
) ENGINE=InnoDB;


alter table materias modify id int not null AUTO_INCREMENT PRIMARY KEY
--drop table materias_plan_estudio;
CREATE TABLE materias_plan_estudio(
id int not null AUTO_INCREMENT PRIMARY KEY,
estatus_materias character varying(1) NOT NULL DEFAULT 'C', -- estatus: C = 'cursada', A = 'aprobada', R = 'reprobada', P = 'pendiente'
materias_id int not null,
plan_estudio_id int not null,
usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'

, CONSTRAINT fk_materias_plan_estudio_materias_materias_id 
	FOREIGN KEY (materias_id) REFERENCES materias(id)
	ON DELETE CASCADE ON UPDATE CASCADE
, CONSTRAINT fk_materias_plan_estudio_plan_estudio_plan_estudio_id
	FOREIGN KEY (plan_estudio_id) REFERENCES plan_estudio(id)
	ON DELETE CASCADE ON UPDATE CASCADE
,CONSTRAINT chk_materias_plan_estudio_estatus_materias CHECK (estatus_materias IN ('C','A','R','P'))

) ENGINE=InnoDB;


--drop table cronograma_actividades;
CREATE TABLE cronograma_actividades(
id int not null AUTO_INCREMENT PRIMARY KEY,
seccion_id int not null,
plan_estudio_id int not null,
observacion text,
usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'

, CONSTRAINT fk_cronograma_actividades_seccion_seccion_id 
	FOREIGN KEY (seccion_id) REFERENCES seccion(id)
	ON DELETE CASCADE ON UPDATE CASCADE
, CONSTRAINT fk_cronograma_actividades_plan_estudio_plan_estudio_id
	FOREIGN KEY (plan_estudio_id) REFERENCES plan_estudio(id)
	ON DELETE CASCADE ON UPDATE CASCADE

) ENGINE=InnoDB;


--drop table inscripcion;
CREATE TABLE inscripcion(
id int not null AUTO_INCREMENT PRIMARY KEY,
descripcion text not null,
estatus_inscripcion character varying(1) NOT NULL DEFAULT 'I', -- estatus: I = 'inscrito', R = 'retirado', F = 'finalizado', P = 'cursando primer lapso', S = 'cursando segundo lapso', T = 'cursando tercer lapso'
observacion text,
alumnos_id int not null,
representante_id int not null,

cronograma_actividades_id int not null,
usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'

, CONSTRAINT fk_inscripcion_alumnos_alumnos_id 
	FOREIGN KEY (alumnos_id) REFERENCES alumnos(id)
	ON DELETE CASCADE ON UPDATE CASCADE
, CONSTRAINT fk_inscripcion_representante_representante_id 
	FOREIGN KEY (representante_id) REFERENCES representante(id_r)
	ON DELETE CASCADE ON UPDATE CASCADE
, CONSTRAINT fk_inscripcion_cronograma_actividades_cronograma_actividades_id
	FOREIGN KEY (cronograma_actividades_id) REFERENCES cronograma_actividades(id)
	ON DELETE CASCADE ON UPDATE CASCADE
,CONSTRAINT chk_inscripcion_estatus_inscripcion CHECK (estatus_inscripcion IN ('I','R','F','P','S','T'))

	
) ENGINE=InnoDB;

--drop table lapso
CREATE TABLE IF NOT EXISTS `lapso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


--drop table asignacion_nota;
CREATE TABLE asignacion_nota(
id int not null AUTO_INCREMENT PRIMARY KEY,
descripcion text not null,
estatus_asignacion character varying(1) NOT NULL DEFAULT 'C', -- estatus: C = 'cursada', A = 'aprobada', R = 'reprobada', P = 'pendiente'
observacion text,
lapso_id int not null,
materias_id int not null,
profesor_id int not null,
inscripcion_id int not null,
usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'

, CONSTRAINT fk_asignacion_nota_lapso_lapso_id 
	FOREIGN KEY (lapso_id) REFERENCES lapso(id)
	ON DELETE CASCADE ON UPDATE CASCADE
, CONSTRAINT fk_asignacion_nota_materias_materias_id 
	FOREIGN KEY (materias_id) REFERENCES materias(id)
	ON DELETE CASCADE ON UPDATE CASCADE
, CONSTRAINT fk_asignacion_nota_profesor_profesor_id 
	FOREIGN KEY (profesor_id) REFERENCES profesor(id_profesor)
	ON DELETE CASCADE ON UPDATE CASCADE
, CONSTRAINT fk_asignacion_nota_inscripcion_inscripcion_id 
	FOREIGN KEY (inscripcion_id) REFERENCES inscripcion(id)
	ON DELETE CASCADE ON UPDATE CASCADE
,CONSTRAINT chk_inscripcion_estatus_asignacion CHECK (estatus_inscripcion IN ('C','A','R','P'))

	
) ENGINE=InnoDB;


--drop table clase;
CREATE TABLE clase(
id int not null AUTO_INCREMENT PRIMARY KEY,
observacion text,

aula_id int not null,
profesor_id int not null,
hora_clase_id int not null,
materias_id int not null,
cronograma_actividades_id int not null,

usuario_ini_id int NOT NULL, -- Usuario que efectúa el registro inicial
fecha_ini DateTime , -- Fecha en la que se hace el registro inicial.
usuario_act_id int, -- Usuario que actualiza.
fecha_act DateTime, -- fecha en la que se actualiza.
estatus character varying(1) NOT NULL DEFAULT 'A'

, CONSTRAINT fk_clase_aula_aula_id 
	FOREIGN KEY (aula_id) REFERENCES aula(id)
	ON DELETE CASCADE ON UPDATE CASCADE
, CONSTRAINT fk_clase_profesor_profesor_id 
	FOREIGN KEY (profesor_id) REFERENCES profesor(id_profesor)
	ON DELETE CASCADE ON UPDATE CASCADE
, CONSTRAINT fk_clase_hora_clase_hora_clase_id 
	FOREIGN KEY (hora_clase_id) REFERENCES hora_clase(id)
	ON DELETE CASCADE ON UPDATE CASCADE
, CONSTRAINT fk_clase_materias_materias_id 
	FOREIGN KEY (materias_id) REFERENCES materias(id)
	ON DELETE CASCADE ON UPDATE CASCADE
, CONSTRAINT fk_clase_cronograma_actividades_cronograma_actividades_id 
	FOREIGN KEY (cronograma_actividades_id) REFERENCES cronograma_actividades(id)
	ON DELETE CASCADE ON UPDATE CASCADE

) ENGINE=InnoDB;


--------------------------------------------------------------------------------------------------->
ALTER TABLE clase ADD dia_id int not null;

ALTER table clase ADD CONSTRAINT fk_clase_dia_dia_id
FOREIGN KEY (dia_id) REFERENCES dia(id)
ON DELETE CASCADE ON UPDATE CASCADE 
; 

ALTER TABLE clase ADD turno_id int not null;

ALTER table clase ADD CONSTRAINT fk_clase_turno_turno_id
FOREIGN KEY (turno_id) REFERENCES turno(id)
ON DELETE CASCADE ON UPDATE CASCADE 
; 
--------------------------------------------------------------------------------------------------->