CREATE DATABASE  IF NOT EXISTS `liceo_simon_bolivar` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `liceo_simon_bolivar`;
-- MySQL dump 10.13  Distrib 5.5.28, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: liceo_simon_bolivar
-- ------------------------------------------------------
-- Server version	5.5.28-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aprobacion`
--

DROP TABLE IF EXISTS `aprobacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aprobacion` (
  `grado` int(1) NOT NULL,
  `condicion` varchar(4) NOT NULL,
  `id_aprob` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_aprob`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aprobacion`
--

LOCK TABLES `aprobacion` WRITE;
/*!40000 ALTER TABLE `aprobacion` DISABLE KEYS */;
INSERT INTO `aprobacion` VALUES (1,'12',1),(2,'12',2),(3,'12',3),(4,'14',4),(5,'7',5);
/*!40000 ALTER TABLE `aprobacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alumnos`
--

DROP TABLE IF EXISTS `alumnos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumnos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `cedula` int(11) NOT NULL,
  `telf` int(12) NOT NULL,
  `direccion` text NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `edad` int(11) NOT NULL,
  `fecha_nac` date NOT NULL,
  `estatura` varchar(5) NOT NULL,
  `peso` int(5) NOT NULL,
  `lugar_nac` text NOT NULL,
  `correo` varchar(80) NOT NULL,
  `ano_curso` int(2) NOT NULL,
  `periodo` varchar(20) NOT NULL,
  `estatus` varchar(8) NOT NULL,
  `seccion` varchar(4) NOT NULL,
  `id_representante` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumnos`
--

LOCK TABLES `alumnos` WRITE;
/*!40000 ALTER TABLE `alumnos` DISABLE KEYS */;
INSERT INTO `alumnos` VALUES (28,'laynerker','guerrero',20308878,2147483647,'pruebas','M',21,'1990-08-08','1.65',12,'caracas','laynerker.gdl@gmail.com',2,'2012-2013','1','C',7,NULL),(29,'lay','prato',345,0,'ASDASDASDASDA','M',12,'2000-06-13','1.47',2,'caracas','laynerker@hola.com',3,'2012-2013','1','E',7,NULL),(33,'Jean Carlos','HernÃ¡ndez Torres',15331426,0,'Prueba','M',19,'1997-02-19','1.50',83,'La Azulita, Edo. MÃ©rida','jeanchernandez@gmail.com',5,'2012-2013','3','B',11,NULL),(34,'Juan Carlos','Sotillo DurÃ¡n',18554785,2147483647,'Prueba','M',16,'1998-03-20','1.50',82,'La Azulita, Edo. MÃ©rida','jsotillo@gmail.com',5,'2012-2013','3','A',11,NULL),(35,'Ana','LÃ³pez',12456358,2122589632,'PRUEBA','F',16,'1996-02-01','1.55',50,'Caracas','ana@lopez.com',3,'2012-2013','1','C',12,NULL),(36,'Eyleen','Vitiello',24587586,2122589632,'PRUEBA','F',16,'1996-02-22','1.57',50,'Caracas','jsotillo@gmail.com',1,'2012-2013','1','C',12,NULL);
/*!40000 ALTER TABLE `alumnos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personas`
--

DROP TABLE IF EXISTS `personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personas` (
  `id_personas` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `apellido` varchar(20) NOT NULL,
  `ci` int(10) NOT NULL,
  PRIMARY KEY (`id_personas`),
  UNIQUE KEY `ci` (`ci`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personas`
--

LOCK TABLES `personas` WRITE;
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
INSERT INTO `personas` VALUES (5,'Administrador','Del Sistema',123),(21,'Eduardo','Blanco',13969384),(22,'Dayerlin','Torrealba',12345678),(24,'howard','Ramirez',12000100),(26,'freddy','otero',12000500),(27,'jose','perez',2222222),(29,'Pedro','Perez',12345679),(30,'Pedro','Perez',123454754),(31,'Jireh','Blanco',23000500),(32,'yinny','morales',7392134);
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logins`
--

DROP TABLE IF EXISTS `logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logins` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(35) NOT NULL,
  `estatus` int(1) NOT NULL,
  `perfil` int(1) NOT NULL,
  `id_personas` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logins`
--

LOCK TABLES `logins` WRITE;
/*!40000 ALTER TABLE `logins` DISABLE KEYS */;
INSERT INTO `logins` VALUES (1,'admin','c4ca4238a0b923820dcc509a6f75849b',1,1,5),(14,'EBlanco','202cb962ac59075b964b07152d234b70',1,1,21),(15,'DTorrealba','202cb962ac59075b964b07152d234b70',1,2,22);
/*!40000 ALTER TABLE `logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `representante`
--

DROP TABLE IF EXISTS `representante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `representante` (
  `id_r` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_r` varchar(80) NOT NULL,
  `apellido_r` varchar(80) NOT NULL,
  `cedula_r` int(11) NOT NULL,
  `edad_r` int(2) NOT NULL,
  `fecha_nac_r` date NOT NULL,
  `telefono_r` int(12) NOT NULL,
  `trabajo_r` varchar(80) NOT NULL,
  `direccion_trabajo_r` text NOT NULL,
  `correo_r` varchar(80) NOT NULL,
  `parentesco` varchar(20) NOT NULL,
  `sexo_r` char(1) NOT NULL DEFAULT 'F',
  PRIMARY KEY (`id_r`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `representante`
--

LOCK TABLES `representante` WRITE;
/*!40000 ALTER TABLE `representante` DISABLE KEYS */;
INSERT INTO `representante` VALUES (7,'Ana','Contreras',123456,30,'2009-11-10',12345,'insopesca','sed','ana@ana.com','Madre','F'),(8,'ana','contreras',34534,23,'1989-04-12',12345,'insopesca','chacaito','laynerker@hot.com','Madre','F'),(11,'Prueba','Prueba',14256325,52,'1960-02-01',2125487948,'Prueba','Prueba MODIFICACIÃ“N','prueba@prueba.test','Madre','F'),(12,'Pedro','LÃ³pez',7586145,52,'1960-02-18',2125487948,'Prueba','Dir de Prueba','pedro@lopez.com','Padre','F');
/*!40000 ALTER TABLE `representante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datos_alumnos`
--

DROP TABLE IF EXISTS `datos_alumnos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datos_alumnos` (
  `id_alumno` int(11) NOT NULL,
  `periodo` varchar(10) NOT NULL,
  `materias_cursadas` int(3) NOT NULL,
  `materias_aprobadas` int(4) NOT NULL,
  `aprobacion` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_alumno`,`periodo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datos_alumnos`
--

LOCK TABLES `datos_alumnos` WRITE;
/*!40000 ALTER TABLE `datos_alumnos` DISABLE KEYS */;
INSERT INTO `datos_alumnos` VALUES (1,'2012-2013',12,0,NULL),(17,'12',12,0,NULL),(18,'12',12,0,NULL),(19,'12',12,0,NULL),(20,'12',12,0,NULL),(21,'12',12,0,NULL),(22,'12',12,0,NULL),(23,'12',12,0,NULL),(24,'12',12,0,NULL),(25,'12',12,0,NULL),(26,'12',12,0,NULL),(27,'12',12,0,NULL),(28,'2012-2013',12,0,NULL),(29,'2012-2013',12,0,NULL),(30,'2012-2013',12,0,NULL),(31,'2012-2013',12,0,NULL),(32,'2012-2013',12,0,NULL),(33,'2012-2013',12,12,1),(33,'2013-2014',12,12,1),(33,'2014-2015',12,12,1),(33,'2015-2016',12,12,1),(34,'2011-2012',7,7,1),(35,'2012-2013',12,0,NULL),(36,'2012-2013',12,0,NULL);
/*!40000 ALTER TABLE `datos_alumnos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-11-06 18:23:20
