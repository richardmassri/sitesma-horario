-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 17, 2016 at 02:53 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `liceo_4feb`
--

-- --------------------------------------------------------

--
-- Table structure for table `alumnos`
--

CREATE TABLE IF NOT EXISTS `alumnos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `cedula` int(11) NOT NULL,
  `telf` varchar(15) NOT NULL,
  `direccion` text NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `edad` int(11) NOT NULL,
  `fecha_nac` date NOT NULL,
  `estatura` varchar(5) NOT NULL,
  `peso` int(5) NOT NULL,
  `lugar_nac` text NOT NULL,
  `correo` varchar(80) NOT NULL,
  `ano_curso` int(2) NOT NULL,
  `periodo` varchar(20) NOT NULL,
  `estatus` varchar(8) NOT NULL,
  `seccion` varchar(4) NOT NULL,
  `id_representante` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `alumnos`
--

INSERT INTO `alumnos` (`id`, `nombre`, `apellido`, `cedula`, `telf`, `direccion`, `sexo`, `edad`, `fecha_nac`, `estatura`, `peso`, `lugar_nac`, `correo`, `ano_curso`, `periodo`, `estatus`, `seccion`, `id_representante`, `id_usuario`) VALUES
(37, 'Jose', 'Gonzalez', 20411263, '20145789152', 'Los Teques', 'M', 15, '2016-01-17', '1.55', 70, 'Los Teques', 'jose@gmail.com', 1, '1', 'A', 'A', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `anio_escolar`
--

CREATE TABLE IF NOT EXISTS `anio_escolar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anio` year(4) NOT NULL,
  `observacion` text,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `anio_escolar`
--

INSERT INTO `anio_escolar` (`id`, `anio`, `observacion`, `usuario_ini_id`, `fecha_ini`, `usuario_act_id`, `fecha_act`, `estatus`) VALUES
(1, 2014, 'Apertura del Año Escolar 2014', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(2, 2015, 'Cierre del Año Escolar 2014 y\r\nApertura del Año Escolar 2015', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(3, 2016, 'Apertura del Año Escolar 2016', 1, '2016-01-17 00:00:00', NULL, NULL, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `ano1`
--

CREATE TABLE IF NOT EXISTS `ano1` (
  `id_ano1` int(5) NOT NULL AUTO_INCREMENT,
  `cedula` int(10) NOT NULL,
  `cl` int(5) NOT NULL,
  `in` int(5) NOT NULL,
  `ma` int(5) NOT NULL,
  `en` int(5) NOT NULL,
  `hv` int(5) NOT NULL,
  `ef` int(5) NOT NULL,
  `gg` int(5) NOT NULL,
  `ea` int(5) NOT NULL,
  `ed` int(5) NOT NULL,
  `et` int(5) NOT NULL,
  `cl2` int(5) NOT NULL,
  `in2` int(5) NOT NULL,
  `ma2` int(5) NOT NULL,
  `en2` int(5) NOT NULL,
  `hv2` int(5) NOT NULL,
  `ef2` int(5) NOT NULL,
  `gg2` int(5) NOT NULL,
  `ea2` int(5) NOT NULL,
  `ed2` int(5) NOT NULL,
  `et2` int(5) NOT NULL,
  `cl3` int(5) NOT NULL,
  `in3` int(5) NOT NULL,
  `ma3` int(5) NOT NULL,
  `en3` int(5) NOT NULL,
  `hv3` int(5) NOT NULL,
  `ef3` int(5) NOT NULL,
  `gg3` int(5) NOT NULL,
  `ea3` int(5) NOT NULL,
  `ed3` int(5) NOT NULL,
  `et3` int(5) NOT NULL,
  PRIMARY KEY (`id_ano1`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `ano2`
--

CREATE TABLE IF NOT EXISTS `ano2` (
  `cl` int(5) DEFAULT NULL,
  `in` int(5) DEFAULT NULL,
  `ma` int(5) DEFAULT NULL,
  `es` int(5) DEFAULT NULL,
  `cb` int(5) DEFAULT NULL,
  `hv` int(5) DEFAULT NULL,
  `hu` int(5) DEFAULT NULL,
  `ea` int(5) DEFAULT NULL,
  `ef` int(5) DEFAULT NULL,
  `et` int(5) DEFAULT NULL,
  `cl2` int(5) DEFAULT NULL,
  `in2` int(5) DEFAULT NULL,
  `ma2` int(5) DEFAULT NULL,
  `es2` int(5) DEFAULT NULL,
  `cb2` int(5) DEFAULT NULL,
  `hv2` int(5) DEFAULT NULL,
  `hu2` int(5) DEFAULT NULL,
  `ea2` int(5) DEFAULT NULL,
  `ef2` int(5) DEFAULT NULL,
  `et2` int(5) DEFAULT NULL,
  `cl3` int(5) DEFAULT NULL,
  `in3` int(5) DEFAULT NULL,
  `ma3` int(5) DEFAULT NULL,
  `es3` int(5) DEFAULT NULL,
  `cb3` int(5) DEFAULT NULL,
  `hv3` int(5) DEFAULT NULL,
  `hu3` int(5) DEFAULT NULL,
  `ea3` int(5) DEFAULT NULL,
  `ef3` int(5) DEFAULT NULL,
  `et3` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ano3`
--

CREATE TABLE IF NOT EXISTS `ano3` (
  `cl` int(5) DEFAULT NULL,
  `in` int(5) DEFAULT NULL,
  `ma` int(5) DEFAULT NULL,
  `cb` int(5) DEFAULT NULL,
  `fi` int(5) DEFAULT NULL,
  `qu` int(5) DEFAULT NULL,
  `hv` int(5) DEFAULT NULL,
  `gv` int(5) DEFAULT NULL,
  `ef` int(5) DEFAULT NULL,
  `et` int(5) DEFAULT NULL,
  `cl2` int(5) DEFAULT NULL,
  `in2` int(5) DEFAULT NULL,
  `ma2` int(5) DEFAULT NULL,
  `cb2` int(5) DEFAULT NULL,
  `fi2` int(5) DEFAULT NULL,
  `qu2` int(5) DEFAULT NULL,
  `hv2` int(5) DEFAULT NULL,
  `gv2` int(5) DEFAULT NULL,
  `ef2` int(5) DEFAULT NULL,
  `et2` int(5) DEFAULT NULL,
  `cl3` int(5) DEFAULT NULL,
  `in3` int(5) DEFAULT NULL,
  `ma3` int(5) DEFAULT NULL,
  `cb3` int(5) DEFAULT NULL,
  `fi3` int(5) DEFAULT NULL,
  `qu3` int(5) DEFAULT NULL,
  `hv3` int(5) DEFAULT NULL,
  `gv3` int(5) DEFAULT NULL,
  `ef3` int(5) DEFAULT NULL,
  `et3` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ano4`
--

CREATE TABLE IF NOT EXISTS `ano4` (
  `cl` int(5) DEFAULT NULL,
  `ma` int(5) DEFAULT NULL,
  `hc` int(5) DEFAULT NULL,
  `in` int(5) DEFAULT NULL,
  `ef` int(5) DEFAULT NULL,
  `fi` int(5) DEFAULT NULL,
  `qu` int(5) DEFAULT NULL,
  `cb` int(5) DEFAULT NULL,
  `dt` int(5) DEFAULT NULL,
  `fl` int(5) DEFAULT NULL,
  `ip` int(5) DEFAULT NULL,
  `cl2` int(5) DEFAULT NULL,
  `ma2` int(5) DEFAULT NULL,
  `hc2` int(5) DEFAULT NULL,
  `in2` int(5) DEFAULT NULL,
  `ef2` int(5) DEFAULT NULL,
  `fi2` int(5) DEFAULT NULL,
  `qu2` int(5) DEFAULT NULL,
  `cb2` int(5) DEFAULT NULL,
  `dt2` int(5) DEFAULT NULL,
  `fl2` int(5) DEFAULT NULL,
  `ip2` int(5) DEFAULT NULL,
  `cl3` int(5) DEFAULT NULL,
  `ma3` int(5) DEFAULT NULL,
  `hc3` int(5) DEFAULT NULL,
  `in3` int(5) DEFAULT NULL,
  `ef3` int(5) DEFAULT NULL,
  `fi3` int(5) DEFAULT NULL,
  `qu3` int(5) DEFAULT NULL,
  `cb3` int(5) DEFAULT NULL,
  `dt3` int(5) DEFAULT NULL,
  `fl3` int(5) DEFAULT NULL,
  `ip3` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ano5`
--

CREATE TABLE IF NOT EXISTS `ano5` (
  `in` int(5) DEFAULT NULL,
  `ef` int(5) DEFAULT NULL,
  `ge` int(5) DEFAULT NULL,
  `cl` int(5) DEFAULT NULL,
  `ma` int(5) DEFAULT NULL,
  `fi` int(5) DEFAULT NULL,
  `qu` int(5) DEFAULT NULL,
  `cb` int(5) DEFAULT NULL,
  `ct` int(5) DEFAULT NULL,
  `ip` int(5) DEFAULT NULL,
  `in2` int(5) DEFAULT NULL,
  `ef2` int(5) DEFAULT NULL,
  `ge2` int(5) DEFAULT NULL,
  `cl2` int(5) DEFAULT NULL,
  `ma2` int(5) DEFAULT NULL,
  `fi2` int(5) DEFAULT NULL,
  `qu2` int(5) DEFAULT NULL,
  `cb2` int(5) DEFAULT NULL,
  `ct2` int(5) DEFAULT NULL,
  `ip2` int(5) DEFAULT NULL,
  `in3` int(5) DEFAULT NULL,
  `ef3` int(5) DEFAULT NULL,
  `ge3` int(5) DEFAULT NULL,
  `cl3` int(5) DEFAULT NULL,
  `ma3` int(5) DEFAULT NULL,
  `fi3` int(5) DEFAULT NULL,
  `qu3` int(5) DEFAULT NULL,
  `cb3` int(5) DEFAULT NULL,
  `ct3` int(5) DEFAULT NULL,
  `ip3` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aprobacion`
--

CREATE TABLE IF NOT EXISTS `aprobacion` (
  `grado` int(1) NOT NULL,
  `condicion` varchar(4) NOT NULL,
  `id_aprob` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_aprob`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `aprobacion`
--

INSERT INTO `aprobacion` (`grado`, `condicion`, `id_aprob`) VALUES
(1, '12', 1),
(2, '12', 2),
(3, '12', 3),
(4, '14', 4),
(5, '7', 5);

-- --------------------------------------------------------

--
-- Table structure for table `asignacion_nota`
--

CREATE TABLE IF NOT EXISTS `asignacion_nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `estatus_asignacion` varchar(1) NOT NULL DEFAULT 'C',
  `observacion` text,
  `lapso_id` int(11) NOT NULL,
  `materias_id` int(11) NOT NULL,
  `profesor_id` int(11) NOT NULL,
  `inscripcion_id` int(11) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_asignacion_nota_lapso_lapso_id` (`lapso_id`),
  KEY `fk_asignacion_nota_materias_materias_id` (`materias_id`),
  KEY `fk_asignacion_nota_profesor_profesor_id` (`profesor_id`),
  KEY `fk_asignacion_nota_inscripcion_inscripcion_id` (`inscripcion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `aula`
--

CREATE TABLE IF NOT EXISTS `aula` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `cod_aula` varchar(30) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `aula`
--

INSERT INTO `aula` (`id`, `nombre`, `cod_aula`, `usuario_ini_id`, `fecha_ini`, `usuario_act_id`, `fecha_act`, `estatus`) VALUES
(1, 'salon A1', 'A1', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(2, 'Laboratorio 1', 'LB1', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(3, 'Laboratorio 2', 'LB2', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(4, 'Laboratorio 2', 'LB2', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(5, 'Laboratorio 3', 'LB3', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(6, 'cancha 1', 'cch 1', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(7, 'cancha 2', 'cch 2', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(8, 'salon A2', 'A2', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(9, 'salon A3', 'A3', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(10, 'salon A4', 'A4', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(11, 'salon A5', 'A5', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(12, 'salon A6', 'A6', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(13, 'salon B1', 'B1', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(14, 'salon B2', 'B2', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(15, 'salon B3', 'B3', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(16, 'salon B4', 'B4', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(17, 'salon B5', 'B5', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(18, 'salon B6', 'B6', 1, '2016-01-17 00:00:00', NULL, NULL, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `clase`
--

CREATE TABLE IF NOT EXISTS `clase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `observacion` text,
  `dia_id` int(11) NOT NULL,
  `turno_id` int(11) NOT NULL,
  `aula_id` int(11) NOT NULL,
  `profesor_id` int(11) NOT NULL,
  `hora_clase_id` int(11) NOT NULL,
  `materias_id` int(11) NOT NULL,
  `cronograma_actividades_id` int(11) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_clase_aula_aula_id` (`aula_id`),
  KEY `fk_clase_profesor_profesor_id` (`profesor_id`),
  KEY `fk_clase_hora_clase_hora_clase_id` (`hora_clase_id`),
  KEY `fk_clase_materias_materias_id` (`materias_id`),
  KEY `fk_clase_cronograma_actividades_cronograma_actividades_id` (`cronograma_actividades_id`),
  KEY `fk_clase_dia_dia_id` (`dia_id`),
  KEY `fk_clase_turno_turno_id` (`turno_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `clase`
--

INSERT INTO `clase` (`id`, `observacion`, `dia_id`, `turno_id`, `aula_id`, `profesor_id`, `hora_clase_id`, `materias_id`, `cronograma_actividades_id`, `usuario_ini_id`, `fecha_ini`, `usuario_act_id`, `fecha_act`, `estatus`) VALUES
(2, NULL, 1, 1, 1, 57, 1, 1, 1, 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(3, NULL, 2, 1, 14, 57, 2, 2, 1, 1, '2016-01-17 00:00:00', NULL, NULL, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `cronograma_actividades`
--

CREATE TABLE IF NOT EXISTS `cronograma_actividades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seccion_id` int(11) NOT NULL,
  `plan_estudio_id` int(11) NOT NULL,
  `observacion` text,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_cronograma_actividades_seccion_seccion_id` (`seccion_id`),
  KEY `fk_cronograma_actividades_plan_estudio_plan_estudio_id` (`plan_estudio_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `cronograma_actividades`
--

INSERT INTO `cronograma_actividades` (`id`, `seccion_id`, `plan_estudio_id`, `observacion`, `usuario_ini_id`, `fecha_ini`, `usuario_act_id`, `fecha_act`, `estatus`) VALUES
(1, 1, 1, NULL, 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(2, 1, 2, NULL, 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(3, 2, 1, NULL, 1, '2016-01-17 00:00:00', 0, NULL, 'A'),
(4, 3, 1, NULL, 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(5, 2, 1, NULL, 1, '2016-01-17 00:00:00', 0, NULL, 'A'),
(6, 3, 1, NULL, 1, '2016-01-17 00:00:00', NULL, NULL, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `datos_alumnos`
--

CREATE TABLE IF NOT EXISTS `datos_alumnos` (
  `id_alumno` int(11) NOT NULL,
  `periodo` varchar(10) NOT NULL,
  `materias_cursadas` int(3) NOT NULL,
  `materias_aprobadas` int(4) NOT NULL,
  `aprobacion` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_alumno`,`periodo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dia`
--

CREATE TABLE IF NOT EXISTS `dia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(9) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `dia`
--

INSERT INTO `dia` (`id`, `nombre`, `usuario_ini_id`, `fecha_ini`, `usuario_act_id`, `fecha_act`, `estatus`) VALUES
(1, 'LUNES', 1, '2016-01-16 04:24:25', NULL, NULL, 'A'),
(2, 'MARTES', 1, '2016-01-16 04:24:25', NULL, NULL, 'A'),
(3, 'MIERCOLES', 1, '2016-01-16 04:24:25', NULL, NULL, 'A'),
(4, 'JUEVES', 1, '2016-01-16 04:24:25', NULL, NULL, 'A'),
(5, 'VIERNES', 1, '2016-01-16 04:24:25', NULL, NULL, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `hora`
--

CREATE TABLE IF NOT EXISTS `hora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hora_ini` time NOT NULL,
  `hora_fin` time NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `hora`
--

INSERT INTO `hora` (`id`, `hora_ini`, `hora_fin`, `usuario_ini_id`, `fecha_ini`, `usuario_act_id`, `fecha_act`, `estatus`) VALUES
(1, '07:00:00', '07:45:00', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(2, '07:45:00', '08:30:00', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(3, '08:40:00', '09:25:00', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(4, '09:25:00', '10:10:00', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(5, '10:20:00', '11:05:00', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(6, '11:05:00', '11:50:00', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(7, '01:00:00', '01:45:00', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(8, '01:45:00', '02:30:00', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(9, '02:40:00', '03:25:00', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(10, '03:25:00', '04:10:00', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(11, '04:20:00', '05:05:00', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(12, '05:05:00', '05:50:00', 1, '2016-01-17 00:00:00', NULL, NULL, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `hora_clase`
--

CREATE TABLE IF NOT EXISTS `hora_clase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hora_ini_id` int(11) NOT NULL,
  `hora_fin_id` int(11) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_hora_clase_hora_hora_ini_id` (`hora_ini_id`),
  KEY `fk_hora_clase_hora_hora_fin_id` (`hora_fin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `hora_clase`
--

INSERT INTO `hora_clase` (`id`, `hora_ini_id`, `hora_fin_id`, `usuario_ini_id`, `fecha_ini`, `usuario_act_id`, `fecha_act`, `estatus`) VALUES
(1, 1, 2, 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(2, 3, 4, 1, '2016-01-17 00:00:00', NULL, NULL, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `inscripcion`
--

CREATE TABLE IF NOT EXISTS `inscripcion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `estatus_inscripcion` varchar(1) NOT NULL DEFAULT 'I',
  `observacion` text,
  `alumnos_id` int(11) NOT NULL,
  `representante_id` int(11) NOT NULL,
  `cronograma_actividades_id` int(11) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_inscripcion_alumnos_alumnos_id` (`alumnos_id`),
  KEY `fk_inscripcion_representante_representante_id` (`representante_id`),
  KEY `fk_inscripcion_cronograma_actividades_cronograma_actividades_id` (`cronograma_actividades_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `inscripcion`
--

INSERT INTO `inscripcion` (`id`, `descripcion`, `estatus_inscripcion`, `observacion`, `alumnos_id`, `representante_id`, `cronograma_actividades_id`, `usuario_ini_id`, `fecha_ini`, `usuario_act_id`, `fecha_act`, `estatus`) VALUES
(1, '', 'I', NULL, 37, 13, 1, 1, '2016-01-17 00:00:00', NULL, NULL, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `lapso`
--

CREATE TABLE IF NOT EXISTS `lapso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `lapso`
--

INSERT INTO `lapso` (`id`, `nombre`, `usuario_ini_id`, `fecha_ini`, `usuario_act_id`, `fecha_act`, `estatus`) VALUES
(1, 'PRIMER LAPSO', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(2, 'SEGUNDO LAPSO', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(3, 'TERCER LAPSO', 1, '2016-01-17 00:00:00', NULL, NULL, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE IF NOT EXISTS `logins` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(35) NOT NULL,
  `estatus` int(1) NOT NULL,
  `perfil` int(1) NOT NULL,
  `id_personas` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`id_usuario`, `usuario`, `clave`, `estatus`, `perfil`, `id_personas`) VALUES
(25, 'hbravo', 'e2db52903d3ecc800cd5d719aee01d98', 1, 1, 36),
(26, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 1, 37);

-- --------------------------------------------------------

--
-- Table structure for table `materias`
--

CREATE TABLE IF NOT EXISTS `materias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_mat` varchar(30) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `materias`
--

INSERT INTO `materias` (`id`, `cod_mat`, `descripcion`) VALUES
(1, '1', 'Matematicas'),
(2, 'DIS01', 'DISEÑO'),
(3, 'GEO01', 'GEOGRAFIA'),
(4, 'CAST01', 'CASTELLANO'),
(5, 'INGL01', 'INGLES REDACCION');

-- --------------------------------------------------------

--
-- Table structure for table `materias_plan_estudio`
--

CREATE TABLE IF NOT EXISTS `materias_plan_estudio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estatus_materias` varchar(1) NOT NULL DEFAULT 'C',
  `materias_id` int(11) NOT NULL,
  `plan_estudio_id` int(11) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_materias_plan_estudio_materias_materias_id` (`materias_id`),
  KEY `fk_materias_plan_estudio_plan_estudio_plan_estudio_id` (`plan_estudio_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `materias_plan_estudio`
--

INSERT INTO `materias_plan_estudio` (`id`, `estatus_materias`, `materias_id`, `plan_estudio_id`, `usuario_ini_id`, `fecha_ini`, `usuario_act_id`, `fecha_act`, `estatus`) VALUES
(1, 'C', 1, 1, 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(2, 'C', 2, 1, 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(3, 'C', 1, 2, 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(4, 'C', 2, 2, 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(5, 'C', 3, 2, 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(6, 'C', 4, 2, 1, '2016-01-17 00:00:00', NULL, NULL, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `nivel_academico`
--

CREATE TABLE IF NOT EXISTS `nivel_academico` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `cod_anio` int(2) NOT NULL,
  `descripcion` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `nivel_academico`
--

INSERT INTO `nivel_academico` (`id`, `cod_anio`, `descripcion`) VALUES
(1, 1, 'Primer Año'),
(2, 2, 'Segundo Año'),
(3, 3, 'Tercer Año'),
(4, 4, 'Cuarto Año'),
(5, 5, 'Quinto Año');

-- --------------------------------------------------------

--
-- Table structure for table `periodo`
--

CREATE TABLE IF NOT EXISTS `periodo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `descripcion` text,
  `anio_escolar_ini_id` int(11) NOT NULL,
  `anio_escolar_fin_id` int(11) NOT NULL,
  `fecha_inicio_periodo` datetime NOT NULL,
  `fecha_fin_periodo` datetime NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_periodo_anio_escolar_anio_escolar_ini_id` (`anio_escolar_ini_id`),
  KEY `fk_periodo_anio_escolar_anio_escolar_fin_id` (`anio_escolar_fin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `periodo`
--

INSERT INTO `periodo` (`id`, `nombre`, `descripcion`, `anio_escolar_ini_id`, `anio_escolar_fin_id`, `fecha_inicio_periodo`, `fecha_fin_periodo`, `usuario_ini_id`, `fecha_ini`, `usuario_act_id`, `fecha_act`, `estatus`) VALUES
(1, '2014-2015', '2014-2015', 1, 2, '2014-01-17 00:00:00', '2015-01-17 00:00:00', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(2, '2015-2016', '2015-2016', 2, 3, '2015-01-17 00:00:00', '2016-01-17 00:00:00', 1, '2016-01-17 00:00:00', NULL, NULL, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `personas`
--

CREATE TABLE IF NOT EXISTS `personas` (
  `id_personas` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `apellido` varchar(20) NOT NULL,
  `ci` int(10) NOT NULL,
  PRIMARY KEY (`id_personas`),
  UNIQUE KEY `ci` (`ci`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `personas`
--

INSERT INTO `personas` (`id_personas`, `nombre`, `apellido`, `ci`) VALUES
(36, 'Hernaldo', 'Davila', 8029211),
(37, 'Administrador', 'Administrador', 123456);

-- --------------------------------------------------------

--
-- Table structure for table `plan_estudio`
--

CREATE TABLE IF NOT EXISTS `plan_estudio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `nivel_academico_id` int(11) NOT NULL,
  `periodo_id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `especial_plan_estudio` varchar(1) NOT NULL DEFAULT 'N',
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_plan_estudio_nivel_academico_nivel_academico_id` (`nivel_academico_id`),
  KEY `fk_plan_estudio_periodo_periodo_id` (`periodo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `plan_estudio`
--

INSERT INTO `plan_estudio` (`id`, `nombre`, `nivel_academico_id`, `periodo_id`, `descripcion`, `especial_plan_estudio`, `usuario_ini_id`, `fecha_ini`, `usuario_act_id`, `fecha_act`, `estatus`) VALUES
(1, 'Primer Año - 2014-2015', 1, 1, 'El plan cuenta con todas las materias regulares para los cursantes del Primer Año en el Periodo 2014-2015', 'N', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(2, 'Primer Año - 2014-2015 Especial', 1, 1, 'El plan cuenta con todas las materias regulares para los cursantes del Primer Año en el Periodo 2014-2015', 'S', 1, '2016-01-17 00:00:00', NULL, NULL, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `profesor`
--

CREATE TABLE IF NOT EXISTS `profesor` (
  `id_profesor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `cedula` int(11) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `correo` varchar(80) NOT NULL,
  `materia1` varchar(50) NOT NULL,
  `dh` varchar(50) NOT NULL,
  `materia2` varchar(50) NOT NULL,
  `dh1` varchar(50) NOT NULL,
  PRIMARY KEY (`id_profesor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `profesor`
--

INSERT INTO `profesor` (`id_profesor`, `nombre`, `apellido`, `cedula`, `telefono`, `correo`, `materia1`, `dh`, `materia2`, `dh1`) VALUES
(57, 'NELSON', 'GONZALEZ', 20411163, '04261110963', 'nelso163n@gmail.com', 'matematica', 'abcde1', 'deporte', 'abcde2'),
(58, 'FRAN', 'GUERRERO', 20154785, '04257894153', 'javierg_91@hotmail.com', 'geografia', 'geo1', 'diseño', 'dis1');

-- --------------------------------------------------------

--
-- Table structure for table `profesor_materia`
--

CREATE TABLE IF NOT EXISTS `profesor_materia` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `cedula_prof` int(11) NOT NULL,
  `cod_materia` int(3) NOT NULL,
  `cod_anio` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cedula_prof` (`cedula_prof`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `representante`
--

CREATE TABLE IF NOT EXISTS `representante` (
  `id_r` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_r` varchar(80) NOT NULL,
  `apellido_r` varchar(80) NOT NULL,
  `cedula_r` int(11) NOT NULL,
  `edad_r` int(2) NOT NULL,
  `fecha_nac_r` date NOT NULL,
  `telefono_r` varchar(15) NOT NULL,
  `trabajo_r` varchar(80) NOT NULL,
  `direccion_trabajo_r` text NOT NULL,
  `correo_r` varchar(80) NOT NULL,
  `parentesco` varchar(20) NOT NULL,
  `sexo_r` char(1) NOT NULL DEFAULT 'F',
  PRIMARY KEY (`id_r`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `representante`
--

INSERT INTO `representante` (`id_r`, `nombre_r`, `apellido_r`, `cedula_r`, `edad_r`, `fecha_nac_r`, `telefono_r`, `trabajo_r`, `direccion_trabajo_r`, `correo_r`, `parentesco`, `sexo_r`) VALUES
(13, 'NERZON', 'GONZALEZ', 20411166, 42, '1976-09-15', '2147483647', 'COMERCIANTE', 'CARRIZAL', 'NELSON@GMAIL.COM', 'PADRE', 'F'),
(14, 'MARIA', 'GONZALEZ', 11111222, 42, '1976-09-15', '04261118956', 'COMERCIANTE', 'CARRIZAL', 'MARIA@GMAIL.COM', 'PADRE', 'F');

-- --------------------------------------------------------

--
-- Table structure for table `seccion`
--

CREATE TABLE IF NOT EXISTS `seccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `codigo` varchar(30) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `seccion`
--

INSERT INTO `seccion` (`id`, `nombre`, `codigo`, `usuario_ini_id`, `fecha_ini`, `usuario_act_id`, `fecha_act`, `estatus`) VALUES
(1, 'SECCION A', 'A', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(2, 'SECCION B', 'B', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(3, 'SECCION C', 'C', 1, '2016-01-17 00:00:00', NULL, NULL, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `turno`
--

CREATE TABLE IF NOT EXISTS `turno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(9) NOT NULL,
  `usuario_ini_id` int(11) NOT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `usuario_act_id` int(11) DEFAULT NULL,
  `fecha_act` datetime DEFAULT NULL,
  `estatus` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `turno`
--

INSERT INTO `turno` (`id`, `nombre`, `usuario_ini_id`, `fecha_ini`, `usuario_act_id`, `fecha_act`, `estatus`) VALUES
(1, 'MAÑANA', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(2, 'TARDE', 1, '2016-01-17 00:00:00', NULL, NULL, 'A'),
(3, 'NOCHE', 1, '2016-01-17 00:00:00', NULL, NULL, 'A');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `asignacion_nota`
--
ALTER TABLE `asignacion_nota`
  ADD CONSTRAINT `fk_asignacion_nota_lapso_lapso_id` FOREIGN KEY (`lapso_id`) REFERENCES `lapso` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_asignacion_nota_materias_materias_id` FOREIGN KEY (`materias_id`) REFERENCES `materias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_asignacion_nota_profesor_profesor_id` FOREIGN KEY (`profesor_id`) REFERENCES `profesor` (`id_profesor`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_asignacion_nota_inscripcion_inscripcion_id` FOREIGN KEY (`inscripcion_id`) REFERENCES `inscripcion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `clase`
--
ALTER TABLE `clase`
  ADD CONSTRAINT `fk_clase_aula_aula_id` FOREIGN KEY (`aula_id`) REFERENCES `aula` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_clase_cronograma_actividades_cronograma_actividades_id` FOREIGN KEY (`cronograma_actividades_id`) REFERENCES `cronograma_actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_clase_dia_dia_id` FOREIGN KEY (`dia_id`) REFERENCES `dia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_clase_hora_clase_hora_clase_id` FOREIGN KEY (`hora_clase_id`) REFERENCES `hora_clase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_clase_materias_materias_id` FOREIGN KEY (`materias_id`) REFERENCES `materias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_clase_profesor_profesor_id` FOREIGN KEY (`profesor_id`) REFERENCES `profesor` (`id_profesor`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_clase_turno_turno_id` FOREIGN KEY (`turno_id`) REFERENCES `turno` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cronograma_actividades`
--
ALTER TABLE `cronograma_actividades`
  ADD CONSTRAINT `fk_cronograma_actividades_seccion_seccion_id` FOREIGN KEY (`seccion_id`) REFERENCES `seccion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cronograma_actividades_plan_estudio_plan_estudio_id` FOREIGN KEY (`plan_estudio_id`) REFERENCES `plan_estudio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hora_clase`
--
ALTER TABLE `hora_clase`
  ADD CONSTRAINT `fk_hora_clase_hora_hora_ini_id` FOREIGN KEY (`hora_ini_id`) REFERENCES `hora` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hora_clase_hora_hora_fin_id` FOREIGN KEY (`hora_fin_id`) REFERENCES `hora` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD CONSTRAINT `fk_inscripcion_alumnos_alumnos_id` FOREIGN KEY (`alumnos_id`) REFERENCES `alumnos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_inscripcion_representante_representante_id` FOREIGN KEY (`representante_id`) REFERENCES `representante` (`id_r`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_inscripcion_cronograma_actividades_cronograma_actividades_id` FOREIGN KEY (`cronograma_actividades_id`) REFERENCES `cronograma_actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `materias_plan_estudio`
--
ALTER TABLE `materias_plan_estudio`
  ADD CONSTRAINT `fk_materias_plan_estudio_materias_materias_id` FOREIGN KEY (`materias_id`) REFERENCES `materias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_materias_plan_estudio_plan_estudio_plan_estudio_id` FOREIGN KEY (`plan_estudio_id`) REFERENCES `plan_estudio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periodo`
--
ALTER TABLE `periodo`
  ADD CONSTRAINT `fk_periodo_anio_escolar_anio_escolar_ini_id` FOREIGN KEY (`anio_escolar_ini_id`) REFERENCES `anio_escolar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_periodo_anio_escolar_anio_escolar_fin_id` FOREIGN KEY (`anio_escolar_fin_id`) REFERENCES `anio_escolar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `plan_estudio`
--
ALTER TABLE `plan_estudio`
  ADD CONSTRAINT `fk_plan_estudio_nivel_academico_nivel_academico_id` FOREIGN KEY (`nivel_academico_id`) REFERENCES `nivel_academico` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_plan_estudio_periodo_periodo_id` FOREIGN KEY (`periodo_id`) REFERENCES `periodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
