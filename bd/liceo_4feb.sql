-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 18-08-2015 a las 16:16:41
-- Versión del servidor: 5.5.40
-- Versión de PHP: 5.4.36-0+deb7u1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `liceo_4feb`
--
CREATE DATABASE `liceo_4feb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `liceo_4feb`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE IF NOT EXISTS `alumnos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `cedula` int(11) NOT NULL,
  `telf` int(12) NOT NULL,
  `direccion` text NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `edad` int(11) NOT NULL,
  `fecha_nac` date NOT NULL,
  `estatura` varchar(5) NOT NULL,
  `peso` int(5) NOT NULL,
  `lugar_nac` text NOT NULL,
  `correo` varchar(80) NOT NULL,
  `ano_curso` int(2) NOT NULL,
  `periodo` varchar(20) NOT NULL,
  `estatus` varchar(8) NOT NULL,
  `seccion` varchar(4) NOT NULL,
  `id_representante` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ano1`
--

CREATE TABLE IF NOT EXISTS `ano1` (
  `id_ano1` int(5) NOT NULL AUTO_INCREMENT,
  `cedula` int(10) NOT NULL,
  `cl` int(5) NOT NULL,
  `in` int(5) NOT NULL,
  `ma` int(5) NOT NULL,
  `en` int(5) NOT NULL,
  `hv` int(5) NOT NULL,
  `ef` int(5) NOT NULL,
  `gg` int(5) NOT NULL,
  `ea` int(5) NOT NULL,
  `ed` int(5) NOT NULL,
  `et` int(5) NOT NULL,
  `cl2` int(5) NOT NULL,
  `in2` int(5) NOT NULL,
  `ma2` int(5) NOT NULL,
  `en2` int(5) NOT NULL,
  `hv2` int(5) NOT NULL,
  `ef2` int(5) NOT NULL,
  `gg2` int(5) NOT NULL,
  `ea2` int(5) NOT NULL,
  `ed2` int(5) NOT NULL,
  `et2` int(5) NOT NULL,
  `cl3` int(5) NOT NULL,
  `in3` int(5) NOT NULL,
  `ma3` int(5) NOT NULL,
  `en3` int(5) NOT NULL,
  `hv3` int(5) NOT NULL,
  `ef3` int(5) NOT NULL,
  `gg3` int(5) NOT NULL,
  `ea3` int(5) NOT NULL,
  `ed3` int(5) NOT NULL,
  `et3` int(5) NOT NULL,
  PRIMARY KEY (`id_ano1`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=2;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ano2`
--

CREATE TABLE IF NOT EXISTS `ano2` (
  `cl` int(5) DEFAULT NULL,
  `in` int(5) DEFAULT NULL,
  `ma` int(5) DEFAULT NULL,
  `es` int(5) DEFAULT NULL,
  `cb` int(5) DEFAULT NULL,
  `hv` int(5) DEFAULT NULL,
  `hu` int(5) DEFAULT NULL,
  `ea` int(5) DEFAULT NULL,
  `ef` int(5) DEFAULT NULL,
  `et` int(5) DEFAULT NULL,
  `cl2` int(5) DEFAULT NULL,
  `in2` int(5) DEFAULT NULL,
  `ma2` int(5) DEFAULT NULL,
  `es2` int(5) DEFAULT NULL,
  `cb2` int(5) DEFAULT NULL,
  `hv2` int(5) DEFAULT NULL,
  `hu2` int(5) DEFAULT NULL,
  `ea2` int(5) DEFAULT NULL,
  `ef2` int(5) DEFAULT NULL,
  `et2` int(5) DEFAULT NULL,
  `cl3` int(5) DEFAULT NULL,
  `in3` int(5) DEFAULT NULL,
  `ma3` int(5) DEFAULT NULL,
  `es3` int(5) DEFAULT NULL,
  `cb3` int(5) DEFAULT NULL,
  `hv3` int(5) DEFAULT NULL,
  `hu3` int(5) DEFAULT NULL,
  `ea3` int(5) DEFAULT NULL,
  `ef3` int(5) DEFAULT NULL,
  `et3` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ano3`
--

CREATE TABLE IF NOT EXISTS `ano3` (
  `cl` int(5) DEFAULT NULL,
  `in` int(5) DEFAULT NULL,
  `ma` int(5) DEFAULT NULL,
  `cb` int(5) DEFAULT NULL,
  `fi` int(5) DEFAULT NULL,
  `qu` int(5) DEFAULT NULL,
  `hv` int(5) DEFAULT NULL,
  `gv` int(5) DEFAULT NULL,
  `ef` int(5) DEFAULT NULL,
  `et` int(5) DEFAULT NULL,
  `cl2` int(5) DEFAULT NULL,
  `in2` int(5) DEFAULT NULL,
  `ma2` int(5) DEFAULT NULL,
  `cb2` int(5) DEFAULT NULL,
  `fi2` int(5) DEFAULT NULL,
  `qu2` int(5) DEFAULT NULL,
  `hv2` int(5) DEFAULT NULL,
  `gv2` int(5) DEFAULT NULL,
  `ef2` int(5) DEFAULT NULL,
  `et2` int(5) DEFAULT NULL,
  `cl3` int(5) DEFAULT NULL,
  `in3` int(5) DEFAULT NULL,
  `ma3` int(5) DEFAULT NULL,
  `cb3` int(5) DEFAULT NULL,
  `fi3` int(5) DEFAULT NULL,
  `qu3` int(5) DEFAULT NULL,
  `hv3` int(5) DEFAULT NULL,
  `gv3` int(5) DEFAULT NULL,
  `ef3` int(5) DEFAULT NULL,
  `et3` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ano4`
--

CREATE TABLE IF NOT EXISTS `ano4` (
  `cl` int(5) DEFAULT NULL,
  `ma` int(5) DEFAULT NULL,
  `hc` int(5) DEFAULT NULL,
  `in` int(5) DEFAULT NULL,
  `ef` int(5) DEFAULT NULL,
  `fi` int(5) DEFAULT NULL,
  `qu` int(5) DEFAULT NULL,
  `cb` int(5) DEFAULT NULL,
  `dt` int(5) DEFAULT NULL,
  `fl` int(5) DEFAULT NULL,
  `ip` int(5) DEFAULT NULL,
  `cl2` int(5) DEFAULT NULL,
  `ma2` int(5) DEFAULT NULL,
  `hc2` int(5) DEFAULT NULL,
  `in2` int(5) DEFAULT NULL,
  `ef2` int(5) DEFAULT NULL,
  `fi2` int(5) DEFAULT NULL,
  `qu2` int(5) DEFAULT NULL,
  `cb2` int(5) DEFAULT NULL,
  `dt2` int(5) DEFAULT NULL,
  `fl2` int(5) DEFAULT NULL,
  `ip2` int(5) DEFAULT NULL,
  `cl3` int(5) DEFAULT NULL,
  `ma3` int(5) DEFAULT NULL,
  `hc3` int(5) DEFAULT NULL,
  `in3` int(5) DEFAULT NULL,
  `ef3` int(5) DEFAULT NULL,
  `fi3` int(5) DEFAULT NULL,
  `qu3` int(5) DEFAULT NULL,
  `cb3` int(5) DEFAULT NULL,
  `dt3` int(5) DEFAULT NULL,
  `fl3` int(5) DEFAULT NULL,
  `ip3` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ano5`
--

CREATE TABLE IF NOT EXISTS `ano5` (
  `in` int(5) DEFAULT NULL,
  `ef` int(5) DEFAULT NULL,
  `ge` int(5) DEFAULT NULL,
  `cl` int(5) DEFAULT NULL,
  `ma` int(5) DEFAULT NULL,
  `fi` int(5) DEFAULT NULL,
  `qu` int(5) DEFAULT NULL,
  `cb` int(5) DEFAULT NULL,
  `ct` int(5) DEFAULT NULL,
  `ip` int(5) DEFAULT NULL,
  `in2` int(5) DEFAULT NULL,
  `ef2` int(5) DEFAULT NULL,
  `ge2` int(5) DEFAULT NULL,
  `cl2` int(5) DEFAULT NULL,
  `ma2` int(5) DEFAULT NULL,
  `fi2` int(5) DEFAULT NULL,
  `qu2` int(5) DEFAULT NULL,
  `cb2` int(5) DEFAULT NULL,
  `ct2` int(5) DEFAULT NULL,
  `ip2` int(5) DEFAULT NULL,
  `in3` int(5) DEFAULT NULL,
  `ef3` int(5) DEFAULT NULL,
  `ge3` int(5) DEFAULT NULL,
  `cl3` int(5) DEFAULT NULL,
  `ma3` int(5) DEFAULT NULL,
  `fi3` int(5) DEFAULT NULL,
  `qu3` int(5) DEFAULT NULL,
  `cb3` int(5) DEFAULT NULL,
  `ct3` int(5) DEFAULT NULL,
  `ip3` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprobacion`
--

CREATE TABLE IF NOT EXISTS `aprobacion` (
  `grado` int(1) NOT NULL,
  `condicion` varchar(4) NOT NULL,
  `id_aprob` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_aprob`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `aprobacion`
--

INSERT INTO `aprobacion` (`grado`, `condicion`, `id_aprob`) VALUES
(1, '12', 1),
(2, '12', 2),
(3, '12', 3),
(4, '14', 4),
(5, '7', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_alumnos`
--

CREATE TABLE IF NOT EXISTS `datos_alumnos` (
  `id_alumno` int(11) NOT NULL,
  `periodo` varchar(10) NOT NULL,
  `materias_cursadas` int(3) NOT NULL,
  `materias_aprobadas` int(4) NOT NULL,
  `aprobacion` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_alumno`,`periodo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logins`
--

CREATE TABLE IF NOT EXISTS `logins` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(35) NOT NULL,
  `estatus` int(1) NOT NULL,
  `perfil` int(1) NOT NULL,
  `id_personas` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Volcado de datos para la tabla `logins`
--

INSERT INTO `logins` (`id_usuario`, `usuario`, `clave`, `estatus`, `perfil`, `id_personas`) VALUES
(25, 'hbravo', 'e2db52903d3ecc800cd5d719aee01d98', 1, 1, 36),
(26, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 1, 37);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

CREATE TABLE IF NOT EXISTS `materias` (
  `id` int(2) NOT NULL,
  `cod_mat` int(2) NOT NULL,
  `descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`id`, `cod_mat`, `descripcion`) VALUES
(0, 1, 'Matematicas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel_academico`
--

CREATE TABLE IF NOT EXISTS `nivel_academico` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `cod_anio` int(2) NOT NULL,
  `descripcion` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `nivel_academico`
--

INSERT INTO `nivel_academico` (`id`, `cod_anio`, `descripcion`) VALUES
(1, 1, 'Primer Año'),
(2, 2, 'Segundo Año');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE IF NOT EXISTS `personas` (
  `id_personas` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `apellido` varchar(20) NOT NULL,
  `ci` int(10) NOT NULL,
  PRIMARY KEY (`id_personas`),
  UNIQUE KEY `ci` (`ci`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id_personas`, `nombre`, `apellido`, `ci`) VALUES
(36, 'Hernaldo', 'Davila', 8029211),
(37, 'Administrador', 'Administrador', 123456);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE IF NOT EXISTS `profesor` (
  `id_profesor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `cedula` int(11) NOT NULL,
  `telefono` int(12) NOT NULL,
  `correo` varchar(80) NOT NULL,
  `materia1` varchar(50) NOT NULL,
  `dh` varchar(50) NOT NULL,
  `materia2` varchar(50) NOT NULL,
  `dh1` varchar(50) NOT NULL,
  PRIMARY KEY (`id_profesor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor_materia`
--

CREATE TABLE IF NOT EXISTS `profesor_materia` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `cedula_prof` int(11) NOT NULL,
  `cod_materia` int(3) NOT NULL,
  `cod_anio` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cedula_prof` (`cedula_prof`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `representante`
--

CREATE TABLE IF NOT EXISTS `representante` (
  `id_r` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_r` varchar(80) NOT NULL,
  `apellido_r` varchar(80) NOT NULL,
  `cedula_r` int(11) NOT NULL,
  `edad_r` int(2) NOT NULL,
  `fecha_nac_r` date NOT NULL,
  `telefono_r` int(12) NOT NULL,
  `trabajo_r` varchar(80) NOT NULL,
  `direccion_trabajo_r` text NOT NULL,
  `correo_r` varchar(80) NOT NULL,
  `parentesco` varchar(20) NOT NULL,
  `sexo_r` char(1) NOT NULL DEFAULT 'F',
  PRIMARY KEY (`id_r`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
