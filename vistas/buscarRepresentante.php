<?php
	require_once("../php/conexion.php");

	$cedula = $_GET['term'];
	$sql = "SELECT
				`representante`.`id_r`,
				`representante`.`nombre_r`,
				`representante`.`apellido_r`,
				`representante`.`cedula_r`,
				`representante`.`edad_r`,
				`representante`.`fecha_nac_r`,
				`representante`.`telefono_r`,
				`representante`.`trabajo_r`,
				`representante`.`direccion_trabajo_r`,
				`representante`.`correo_r`,
				`representante`.`parentesco`,
				`representante`.`sexo_r`
			FROM 
				`liceo_simon_bolivar`.`representante`
			WHERE 
				CAST(cedula_r AS CHAR) LIKE '%".$cedula."%'";
	
	$res = array();
	$consulta=mysql_query($sql);
	$num=mysql_num_rows($consulta);
	if($num != 0)
		while ($arry = mysql_fetch_array($consulta))
			array_push( $res, array(
				'id'					=> $arry['id_r'],
				'label'					=> 'CI: '.$arry['cedula_r'].'; Nombre: '.$arry['nombre_r'].' '.$arry['apellido_r'],
				'value'					=> $arry['cedula_r'],
				'id_r'					=> $arry['id_r'],
				'nombre_r'				=> $arry['nombre_r'],
				'apellido_r'			=> $arry['apellido_r'],
				'cedula_r'  			=> $arry['cedula_r'],
				'edad_r'				=> $arry['edad_r'],
				'fecha_nac_r'			=> $arry['fecha_nac_r'],
				'telf_r'				=> $arry['telefono_r'],
				'trabajo_r'				=> $arry['trabajo_r'],
				'lugar_trabajo_r'		=> $arry['direccion_trabajo_r'],
				'correo_r'				=> $arry['correo_r'],
				'parentesco_r'			=> $arry['parentesco'],
				'sexo_r'				=> $arry['sexo_r']
			) );
	
	echo json_encode($res);

?>