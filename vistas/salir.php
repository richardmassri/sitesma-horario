<?php
	session_start();
	
	if(isset($_SESSION['user']) and isset($_SESSION['clave'])){
		unset($_SESSION['user']);
		unset($_SESSION['clave']);
		unset($_SESSION['perfil']);
	}

	header('Location: ../');
?>