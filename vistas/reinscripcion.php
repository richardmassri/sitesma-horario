<?php include('../php/functiones.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<title>Reinscripción</title>
		<link type="text/css" href="../jquery/css/smoothness/jquery-ui-1.9.0.custom.min.css" rel="stylesheet" />
		<link type="text/css" href="../jquery/css/demos.css" rel="stylesheet" />
		
		<script type="text/javascript" src="../jquery/js/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="../jquery/js/jquery-ui-1.9.0.custom.min.js"></script>
		<script type="text/javascript" src="../jquery/js/jquery.ui.datepicker-es.js"></script>
		
		<script type="text/javascript" src="../js/funciones.js"></script>
		<script type="text/javascript" src="../js/funciones2.js"></script>
		<style>
			.label{ font-weight: bold; }
			tr{ height:25px; }
			.rojo{ color: #f22; }
			.verde{ color: #4c4; }
		</style>
		<script type="text/javascript">
			$(function(){
				$('#materias_aprobadas')
					.keypress(function(e){
						return( IsNumber(e) );
					})
					.change(function(e){
						var mCursadas = parseInt( $('#materias_cursadas').val() );
						var mAprobadas = $(this).val();
						var enCurso = parseInt( $('#ano_curso').val() );
						
						if( mAprobadas>mCursadas ){
							alert('No puede aprobar más materias de las cursadas');
						}else if( mAprobadas<mCursadas-1 || ( mAprobadas==mCursadas-1 && enCurso==5 ) ){
							$('#ano_cursar').val( enCurso );
							$('#status').html('Repitiente').removeClass('verde').addClass('rojo');
						}else if( mAprobadas==mCursadas-1 ){
							$('#ano_cursar').val( enCurso+1 );
							$('#status').removeClass('rojo').addClass('verde').html('Año aprobado <span class="rojo">(con materia de arrastre)</span>');
						}else{
							$('#ano_cursar').val( enCurso+1 );
							$('#status').html('Año aprobado').removeClass('rojo').addClass('verde');
						}
					});
				
				$('#reinscribir')
					.click(function(){
						if(confirm('¿Seguro que desea guardar los valores ingresados?')){
							$('#frmReinscripcion').submit();
						}
					});
			});
		</script>
</head>
	<body>
		<?php
			$alumno = buscar($_GET['id'], 2);
		?>
		<h3 style="margin: 1.5em 0 2em 0 !important; text-align: center;">REINSCRIPCIÓN DEL ALUMNO</h3>
		<form id="frmReinscripcion" action="" method="post">
			<input type="hidden" id="id_alumno" name="id_alumno" value="<?php echo $_GET['id']; ?>" />
			<table width="100%" align="center" style="font-size: 13px;">
				<tr>
					<td class="label" width="25%">Cédula</td>
					<td id="ci" width="25%"><?php echo $alumno['cedula']; ?></td>
					<td class="label" width="25%">Período</td>
					<td width="25%">
						<select id="per" name="per">
							<option value=""  <?php  echo !@$alumno['periodo'] ? 'selected="selected"' : '' ?>>...</option>
							<?php
							
								$anyo = intval( Date("Y") );
								$anyoP= array_shift(array_reverse(explode('2013-2014',$alumno['periodo'])));
								// Se coloca el período cursado deshabilitado para que sepa que es el período que está cursando y
								// los siguientes 3 períodos al año actual o período actual, dependiendo de cuál sea mayor
								$anyo = $anyo<$anyoP ? $anyoP : $anyo;
							?>
							<option value="<?php echo $alumno['periodo']; ?>" disabled="disabled" style="font-weight:bold; text-decoration:line-through !important;"><?php echo $alumno['periodo']; ?></option>
							<?php
								for($i=$anyo; $i<=$anyo+3; $i++){
							?>
							<option value="<?php echo $i; ?>-<?php echo $i+1; ?>" <?php  echo $i==$anyo ? 'selected="selected"' : ''; ?> ><?php echo $i; ?>-<?php echo $i+1; ?></option>
							<?php
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Nombres</td>
					<td id="nombres"><?php echo $alumno['nombre']; ?></td>
					<td class="label">Apellidos</td>
					<td id="apellidos"><?php echo $alumno['apellido']; ?></td>
				</tr>
				<tr>
					<td class="label">Materias cursadas</td>
					<td id="mCursadas"><input type="hidden" id="materias_cursadas" name="materias_cursadas" value="<?php echo $alumno['materias_cursadas']; ?>" /><?php echo $alumno['materias_cursadas']; ?></td>
					<td class="label">Materias aprobadas</td>
					<td id="mAprobadas"><input type="text" id="materias_aprobadas" name="materias_aprobadas" value="<?php echo $alumno['materias_cursadas']; ?>" size="3" /></td>
				</tr>
				<tr>
					<td class="label">Año en curso</td>
					<td id="enCurso"><input type="hidden" id="ano_curso" name="ano_curso" value="<?php echo $alumno['ano_curso']; ?>" /><?php echo $alumno['ano_curso']; ?></td>
					<td class="label">Año a cursar</td>
					<td id="aCursar"><input type="text" id="ano_cursar" name="ano_cursar" value="<?php echo $alumno['ano_curso']<5 ? $alumno['ano_curso']+1 : 5; ?>" size="3" readonly="readonly" /></td>
				</tr>
				<tr>
					<td class="label">Sección</td>
					<td>
						<select name="sec">
							<option value="" <?php  echo !@$alumno['seccion'] ? 'selected="selected"' : '' ?>>...</option>
							<option value="A" <?php  echo @$alumno['seccion']=='A' ? 'selected="selected"' : '' ?>>A</option>
							<option value="B" <?php  echo @$alumno['seccion']=='B' ? 'selected="selected"' : '' ?>>B</option>
							<option value="C" <?php  echo @$alumno['seccion']=='C' ? 'selected="selected"' : '' ?>>C</option>
							<option value="D" <?php  echo @$alumno['seccion']=='D' ? 'selected="selected"' : '' ?>>D</option>
							<option value="E" <?php  echo @$alumno['seccion']=='E' ? 'selected="selected"' : '' ?>>E</option>
							<option value="F" <?php  echo @$alumno['seccion']=='F' ? 'selected="selected"' : '' ?>>F</option>
							<option value="G" <?php  echo @$alumno['seccion']=='G' ? 'selected="selected"' : '' ?>>G</option>
							<option value="H" <?php  echo @$alumno['seccion']=='H' ? 'selected="selected"' : '' ?>>H</option>
						</select>
					</td>
					<td class="label">Status</td>
					<td id="status" class="verde label">Año aprobado</td>
				</tr>
			</table>
			<br/>
			<hr size="1" />
			<div align="right">
				<button type="button" id="reinscribir" name="reinscribir">Reinscribir</button>
			</div>
		</form>
	<?php
		//echo '<pre>'.print_r($_POST,true).'</pre>';
		if( @$_POST['id_alumno'] ){
			$res = reinscribir($_POST);
	?>
		<script type="text/javascript">
			$(function(){
				alert('<?php echo $res[1]; ?>');
				<?php
					if($res[0]){
						if($_POST['ano_curso']==5 && $_POST['ano_cursar']==5 && $_POST['materias_cursadas']==$_POST['materias_aprobadas']){
							//Aprobó 5to año y se gradúa
				?>
				$('body').html('<h3 style="margin: 1.5em 0 2em 0 !important; text-align: center;">FELICIDADES, EL ALUMNO ESTÁ GRADUADO</h3>')
				<?php
						}else{
				?>
				location.href = 'reporte.php?id=<?php echo $_POST['id_alumno']; ?>';
				<?php
						}
					}
				?>
			});
		</script>
	<?php
		}
	?>
	</body>
</html>
