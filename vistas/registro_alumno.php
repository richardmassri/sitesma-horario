		<!--Script-->
		<script>
			$(function() {
				$( document ).tooltip({
					track: true,
					position: {
						my: "left bottom", at: "left top", collision: "flipfit"
					}
				});
				
				$( "#datepicker" )
					.datepicker({
						changeMonth: true,
						changeYear: true,
						yearRange: '1990:2001',
						dateFormat: 'dd/mm/yy',
						defaultDate: new Date(1990,01,01)
					})
					.change(function(){
						var fecha1 = $('#datepicker').val();
						fecha1 = fecha1.split('/');
						fecha1 = new Date(fecha1[2]+'-'+fecha1[1]+'-'+fecha1[0]);
						var fecha2 = new Date('<?php echo date('Y-m-d'); ?>');
						//alert(fecha2);
						$('#edad').val( calcularEdad(fecha1, fecha2) );
					});
			});
		</script>
		
		<!--HTML-->
		<body width="640">
		<div align="center">
			<h3 style="margin: 2px 0 0.5em !important;">REGISTRO DEL ALUMNO</h3>
		</div>
		<!--<form action="" method="post" name="frm_alumno" id="frm_alumno" style="margin: 0 !important;">-->
		<input type="hidden" id="id" name="id" value="<?php echo @$alumno['id']; ?>" />
			<table  border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<h4 class="Estilo2" style="margin: 2px 0 0.5em !important;">Datos Personales del Alumno</h4>
					</td>
				</tr>
				<tr>
					<td>Nombres</td>
					<td>
						<input onKeyPress="return soloLetras(event)" type="text" name="nombre" id="nombre" title="Indique los nombres del alumno" value="<?php echo @$alumno['nombre']; ?>" />	
					</td>
					<td>Apellidos</td>
					<td>
						<input onKeyPress="return soloLetras(event)" type="text" name="apellido" id="apellido" title="Indique los apellidos del alumno" value="<?php echo @$alumno['apellido']; ?>" />   
					</td>
				</tr>
				<tr>
					<td>Cedula</td>
					<td><input onKeyPress="return IsNumber(event);"  type="text" name="ci" id="ci" maxlength="8" title="Indique la cédula del alumno" value="<?php echo @$alumno['cedula']; ?>" /></td>
					<td>Sexo</td>
					<td>
						<select name="sexo">
							<option value="" <?php  echo !@$alumno['sexo'] ? 'selected="selected"' : '' ?>>...</option>
							<option value="M" <?php  echo @$alumno['sexo']=='M' ? 'selected="selected"' : '' ?>>M</option>
							<option value="F" <?php  echo @$alumno['sexo']=='F' ? 'selected="selected"' : '' ?>>F</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Fecha de Nacimiento</td>
					<td><input type="text" name="fecha_nac" id="datepicker" value="<?php echo @$alumno['fecha_nac'] ? formatFecha($alumno['fecha_nac']) : ''; ?>" /></td>
					<td>Edad</td>
					<td><input onKeyPress="return IsNumber(event);"  type="text" name="edad" id="edad" maxlength="2" value="<?php echo @$alumno['edad']; ?>" /></td>
				</tr>
				<tr>
					<td>Estatura</td>
					<td>
						<select name="est">
							<option value="" <?php  echo !@$alumno['estatura'] ? 'selected="selected"' : '' ?>>...</option>
							<?php for($i=1.40; $i<=1.85; $i+=.01){ ?>
							<option value="<?php echo strlen( (string)$i )<4 ? (string)$i.'0' : $i; ?>" <?php echo @$alumno['estatura']==( strlen( (string)$i )<4 ? (string)$i.'0' : (string)$i ) ? 'selected="selected"' : '' ?>><?php echo strlen( (string)$i )<4 ? (string)$i.'0' : $i; ?> m</option>
							<?php } ?>
						</select>
					</td>
					<td>Peso</td>
					<td><input type="text" name="peso" id="peso" onKeyPress="return IsNumber(event);"  value="<?php echo @$alumno['peso']; ?>" /></td>
				</tr>
				<tr>
					<td>Lugar de Nacimiento</td>
					<td><input onKeyPress="return soloLetras(event)" type="text" name="lugar_naci" id="lugar_naci" title="Indique la Localidad, Estado de nacimiento del alumno"  value="<?php echo @$alumno['lugar_nac']; ?>" /></td>
					<td>Correo</td>
					<td><input type="text" name="correo" id="correo" value="<?php echo @$alumno['correo']; ?>" /></td>
				</tr>
				<tr>
					<td>Año a cursar</td>
					<td>
						<select name="ano_curs">
							<option value="" <?php  echo !@$alumno['ano_curso'] ? 'selected="selected"' : '' ?>>...</option>
							<option value="1" <?php  echo @$alumno['ano_curso']==1 ? 'selected="selected"' : '' ?>>1&deg;</option>
							<option value="2" <?php  echo @$alumno['ano_curso']==2 ? 'selected="selected"' : '' ?>>2&deg;</option>
							<option value="3" <?php  echo @$alumno['ano_curso']==3 ? 'selected="selected"' : '' ?>>3&deg;</option>
							<option value="4" <?php  echo @$alumno['ano_curso']==4 ? 'selected="selected"' : '' ?>>4&deg;</option>
							<option value="5" <?php  echo @$alumno['ano_curso']==5 ? 'selected="selected"' : '' ?>>5&deg;</option>
						</select>
					</td>
					<td>Periodo</td>
					<!--Observación: los períodos deberían colocarse automáticamente de la siguiente manera: año anterior y año actual, año actual y año siguiente-->
					<td>
						<select name="per">
							<option value=""  <?php  echo !@$alumno['periodo'] ? 'selected="selected"' : '' ?>>...</option>
<?php

	$anyo = intval( Date("Y") );
	//Se coloca el período anterior a la fecha actual del servidor y los 3 períodos siguientes
	for($i=$anyo-1; $i<=$anyo+3; $i++){
?>
							<option value="<?php echo $i; ?>-<?php echo $i+1; ?>" <?php  echo @$alumno['periodo']==( $i.'-'.($i+1) ) ? 'selected="selected"' : '' ?> ><?php echo $i; ?>-<?php echo $i+1; ?></option>
<?php
	}

?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Telefono</td>
					<td><input onKeyPress="return IsNumber(event);"  type="text" name="telf" value="<?php echo @$alumno['correo']; ?>"></td>
					<td>Sección</td>
					<td>
						<select name="sec">
							<option value="" <?php  echo !@$alumno['seccion'] ? 'selected="selected"' : '' ?>>...</option>
							<option value="A" <?php  echo @$alumno['seccion']=='A' ? 'selected="selected"' : '' ?>>A</option>
							<option value="B" <?php  echo @$alumno['seccion']=='B' ? 'selected="selected"' : '' ?>>B</option>
							<option value="C" <?php  echo @$alumno['seccion']=='C' ? 'selected="selected"' : '' ?>>C</option>
							<option value="D" <?php  echo @$alumno['seccion']=='D' ? 'selected="selected"' : '' ?>>D</option>
							<option value="E" <?php  echo @$alumno['seccion']=='E' ? 'selected="selected"' : '' ?>>E</option>
							<option value="F" <?php  echo @$alumno['seccion']=='F' ? 'selected="selected"' : '' ?>>F</option>
							<option value="G" <?php  echo @$alumno['seccion']=='G' ? 'selected="selected"' : '' ?>>G</option>
							<option value="H" <?php  echo @$alumno['seccion']=='H' ? 'selected="selected"' : '' ?>>H</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Dirección</td>
					<td colspan="3"><textarea  name="direccion" id="direccion" title="Dirección de residencia del alumno" style="width: 100%"><?php echo @$alumno['direccion']; ?></textarea></td>
					
				</tr>
				<tr>
					<td colspan="4" align="right">
						<input type="button" value="Siguiente" name="enviar" onClick="if( Verificar() ){ siguiente(); $('.ui-tooltip-content').remove(); };">
					</td>
				</tr>
			</table>
		<!--</form>-->
<form action="registro_alumno.php" method="post" enctype="multipart/form-data">

 

