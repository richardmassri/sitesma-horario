	<!--Script-->
	<script type="text/javascript">
		$(function() {
			$( "#fecha_nac_r" )
				.datepicker({
					changeMonth: true,
					changeYear: true,
					yearRange: '1950:1994',
					defaultDate: new Date(1960,01,01)
				}).change(function(){
					var fecha1 = $(this).val();
					fecha1 = fecha1.split('/');
					fecha1 = new Date(fecha1[2]+'-'+fecha1[1]+'-'+fecha1[0]);
					var fecha2 = new Date('<?php echo date('Y-m-d'); ?>');
					//alert(fecha2);
					$('#edad_r').val( calcularEdad(fecha1, fecha2) );
				});
			
			$('#ci_r').autocomplete({
				source: 'buscarRepresentante.php',
				minlength: 2,
				select: function(event, ui){
					$('#frm_representante *:not(#ci_r, input[type=button])').attr('readonly', true);
					for(prop in ui.item){
						if( $.inArray(prop, ['id','label','value'])==-1 ){
							$('#'+prop).val( ui.item[prop] );
						}
					}
					$('#rNuevo').show();
				}
			});
			
			$('#parentesco_r').change(function(){
				if( $(this).val()=='Madre' )
					$("#sexo_r option[value='F']").attr("selected",true);
				else if( $(this).val()=='Padre' )
					$("#sexo_r option[value='M']").attr("selected",true);
				else
					$("#sexo_r option[value='']").attr("selected",true);
			});
			
			$('#rNuevo').click(function(){
				
				$('#id_r').val('');
				$('#nombre_r').removeAttr('readonly').val('');
				$('#apellido_r').removeAttr('readonly').val('');
				$('#fecha_nac_r').removeAttr('readonly').val('');
				$('#edad_r').removeAttr('readonly').val('');
				$('#trabajo_r').removeAttr('readonly').val('');
				$('#lugar_trabajo_r').removeAttr('readonly').val('');
				$('#telf_r').removeAttr('readonly').val('');
				$('#correo_r').removeAttr('readonly').val('');
				
				$('#parentesco_r')
					.find('option').removeAttr('selected')
					.end()
					.find('option:eq(0)').attr('selected',true);
				
				$('#sexo_r')
					.find('option').removeAttr('selected')
					.end()
					.find('option:eq(0)').attr('selected',true);
					
				$(this).hide();
			});
			
			$('#anterior').click(function(){
				$('#inscripcion').tabs({selected: 0});
			});
			
			$('#enviar').click(function(){
				if( confirm('¿Desea continuar?') ){
					$('#inscripcion').submit();
				}
			});
		});
	</script>
	
	<div id="frm_representante" align="center"><h3 style="margin: 2px 0 0.5em !important;">REGISTRO DEL REPRESENTANTE LEGAL</h3></div>
	<!--<form action="" method="post" name="frm_representante" id="frm_representante">-->
		<input type="hidden" id="id_r" name="id_r" value="<?php echo @$representante['id_r']; ?>">
		<table  border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="4">
					<div align="left"><h4 class="Estilo2">Datos Personales del Representante</h4></div>
				</td>
			</tr>
			<tr>
				<td>Cedula</td>
				<td colspan="3">
					<input onKeyPress="return IsNumber(event);" type="text" name="ci_r" id="ci_r" maxlength="8" value="<?php echo @$representante['cedula_r']; ?>"/>
					<a href="#" id="rNuevo" style="text-decoration: none; display: none; font-family: sans-serif; font-size: 10px; font-weight: bold; color: #900;">[Nuevo]</a>
				</td>
			</tr>
			<tr>
				<td>Nombres</td>
				<td>
					<input onKeyPress="return soloLetras(event)" type="text" name="nombre_r" id="nombre_r" title="Indique los Nombres del representante" value="<?php echo @$representante['nombre_r']; ?>"/>
				</td>
				<td>Apellidos</td>
				<td><input onKeyPress="return soloLetras(event)" type="text" name="apellido_r" id="apellido_r" title="Indique los apellidos del representante" value="<?php echo @$representante['apellido_r']; ?>"/></td>
			</tr>
			<tr>
				<td>Parentesco</td>
				<td>
					<select id="parentesco_r" name="parentesco_r">
						<option value="" <?php  echo !@$representante['parentesco'] ? 'selected="selected"' : '' ?>>...</option>
						<option value="Madre" <?php  echo @$representante['parentesco']=='Madre' ? 'selected="selected"' : '' ?>>Madre</option>
						<option value="Padre" <?php  echo @$representante['parentesco']=='Padre' ? 'selected="selected"' : '' ?>>Padre</option>
						<option value="Abuelo (a)" <?php  echo @$representante['parentesco']=='Abuelo (a)' ? 'selected="selected"' : '' ?>>Abuelo (a)</option>
						<option value="Hermano (a)" <?php  echo @$representante['parentesco']=='Hermano (a)' ? 'selected="selected"' : '' ?>Hermano (a)</option>
						<option value="Tio (a)" <?php  echo @$representante['parentesco']=='Tio (a)' ? 'selected="selected"' : '' ?>>Tio (a)</option>
						<option value="Primo (a)" <?php  echo @$representante['parentesco']=='Primo (a)' ? 'selected="selected"' : '' ?>>Primo (a)</option>
					</select>
				</td>
				<td>Sexo</td>
				<td>
					<select id="sexo_r" name="sexo_r">
						<option value="" <?php  echo !@$representante['sexo_r'] ? 'selected="selected"' : '' ?>>...</option>
						<option value="M" <?php  echo @$representante['sexo_r']=='M' ? 'selected="selected"' : '' ?>>M</option>
						<option value="F" <?php  echo @$representante['sexo_r']=='F' ? 'selected="selected"' : '' ?>>F</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Fecha de Nacimiento</td>
				<td><input type="text" name="fecha_nac_r" id="fecha_nac_r" value="<?php echo @$representante['fecha_nac_r'] ? formatFecha($representante['fecha_nac_r']) : ''; ?>" /></td>
				<td>Edad</td>
				<td><input onKeyPress="return IsNumber(event);" type="text" name="edad_r" id="edad_r" maxlength="2" value="<?php echo @$representante['edad_r']; ?>"/></td>
			</tr>
			<tr>
				<td>Nombre de la Empresa</td>
				<td><input onKeyPress="return soloLetras(event)" type="text" name="trabajo_r" id="trabajo_r" value="<?php echo @$representante['trabajo_r']; ?>" /></td>
				<td>Dirección de Trabajo</td>
				<td><input onKeyPress="return soloLetras(event)" type="text" name="lugar_trabajo_r" id="lugar_trabajo_r" value="<?php echo @$representante['direccion_trabajo_r']; ?>" /></td>
			</tr>
			<tr>
				<td>Telefono</td>
				<td><input onKeyPress="return IsNumber(event);" type="text" id="telf_r" name="telf_r" value="<?php echo @$representante['telefono_r']; ?>"></td>
				<td>Correo</td>
				<td colspan="3"><input type="text" name="correo_r" id="correo_r" value="<?php echo @$representante['correo_r']; ?>" /></td>
			</tr>
			<tr>
				<td colspan="4" align="right">
					<input type="button" value="Anterior" name="anterior" id="anterior">
					<input type="button" value="Guardar" name="enviar" id="enviar" onClick="if( Verificar_representante() ){ finalizar(); }">
				</td>
			</tr>
		</table>
	<!--</form>-->