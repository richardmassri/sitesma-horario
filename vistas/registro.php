<?php
	session_start();
	include("../php/functiones.php");
	
	if(isset($_GET['id']) && empty($_GET['id2'])){
		if( $alumno=buscar($_GET['id'],'2') ){
			$representante = buscar($alumno['id_representante'], 6);
		}
	}
	
	//Eliminación
	if(isset($_GET['id2'])){
		if($_GET['id2'] == 1){
			eliminar($_GET['id']);
		}
	}
	
	//Activación
	if(isset($_GET['id2'])){
		if($_GET['id2'] == 2){
			activar($_GET['id']);
		}
	}
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Formulario de Registro</title>
		<link type="text/css" href="../jquery/css/smoothness/jquery-ui-1.9.0.custom.min.css" rel="stylesheet" />
		<link type="text/css" href="../jquery/css/demos.css" rel="stylesheet" />
		
		<script type="text/javascript" src="../jquery/js/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="../jquery/js/jquery-ui-1.9.0.custom.min.js"></script>
		<script type="text/javascript" src="../jquery/js/jquery.ui.datepicker-es.js"></script>
		
		<script type="text/javascript" src="../js/funciones.js"></script>
		<script type="text/javascript" src="../js/funciones2.js"></script>
		
		<script type="text/javascript">
			function siguiente(){
				$('#inscripcion').tabs({selected: 1});
			}
			$(function(){
				$('#inscripcion').tabs({ cache: true });
			});
		</script>
		
		<style>
			td{
				padding-left:5px;
				padding-top:5px;
			}
			#div1, #div2, #div3, #div4{  
				visibility: hidden;
				color:#FFFFFF;
				background:#006699;
				width: 105px;
				padding: 10px;
				border:double;
				border-color:#FFFFFF;  
			}
			.Estilo2 {
				color: #990000;
				font-weight: bold;
			}
			#inscripcion .ui-tabs-nav{
				display: none !important;
			}
		</style>
	</head>
	<body>
		<?php
			$res = false;
			if( array_key_exists('ci_r', $_POST) ){
				if( $_POST['id'] ){
					$res = modificar_todo($_POST);
				}else{
					if( $res = inscribir($_POST) ){
						unset($_POST);
					}
				}
			}
		?>
		<form id="inscripcion" action="" method="post" >
			<div id="inscripcion">
				<ul>
					<li><a href="#alumno">Datos del alumno</a></li>
					<li><a href="#representante">Datos del representante</a></li>
				</ul>
				<div id="alumno">
				<?php include('registro_alumno.php') ?>
				</div>
				<div id="representante">
				<?php include('representante.php') ?>
				</div>
			</div>
		</form>
		<?php
			if($res){
				?>
				<script>
					$(function(){ 
						alert('<?php echo $res[1]; ?>');
				<?php
					if( !@array_key_exists( 'id', $_POST ) ){
				?>
						location.href = 'reporte.php?id=<?php echo $res[0] ?>'; 
				<?php
					}
				?>
					});
				</script>
				<?php
			}
		?>	
	</body>
</html>