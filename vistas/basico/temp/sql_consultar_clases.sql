SELECT cl.observacion AS clase_observacion, cadt.id AS cronograma_actividades_id, 
cadt.observacion AS cronograma_actividades_observacion, secc.id AS seccion_id, 
secc.nombre AS seccion_nombre, secc.codigo AS seccion_codigo, d.nombre AS dia_nombre, 
t.nombre AS turno_nombre, a.nombre AS aula_nombre, a.cod_aula AS aula_cod_aula, 
prf.nombre AS profesor_nombre, prf.apellido AS profesor_apellido, prf.cedula AS profesor_cedula, 
mat.cod_mat AS materias_cod_mat, mat.descripcion AS materias_descripcion, 
hclasi.hora_ini AS hora_hora_ini, hclasf.hora_fin AS hora_hora_fin, 

cl.dia_id AS clase_dia_id,
hc.hora_ini_id AS hora_clase_hora_ini_id,
hc.hora_fin_id AS hora_clase_hora_fin_id

FROM 
clase cl
LEFT JOIN dia AS d ON d.id = cl.dia_id
LEFT JOIN turno AS t ON t.id = cl.turno_id
LEFT JOIN aula AS a ON a.id = cl.aula_id
LEFT JOIN profesor AS prf ON prf.id_profesor = cl.profesor_id
LEFT JOIN hora_clase AS hc ON hc.id = cl.hora_clase_id
LEFT JOIN materias AS mat ON mat.id = cl.materias_id
LEFT JOIN cronograma_actividades AS cadt ON cadt.id = cl.cronograma_actividades_id
LEFT JOIN seccion AS secc ON secc.id = cadt.seccion_id
LEFT JOIN hora AS hclasi ON hclasi.id = hc.hora_ini_id
LEFT JOIN hora AS hclasf ON hclasf.id = hc.hora_fin_id



SELECT id, hora_ini, hora_fin FROM hora;

SELECT id, nombre FROM turno;




----------------------------------------
SELECT cl.observacion, cadt.id, cadt.observacion, secc.id, secc.nombre, secc.codigo, d.nombre, 
t.nombre, a.nombre, a.cod_aula, prf.nombre, prf.apellido, prf.cedula, 
mat.cod_mat, mat.descripcion, hclasi.hora_ini, hclasf.hora_fin 

FROM 
clase cl
LEFT JOIN dia AS d ON d.id = cl.dia_id
LEFT JOIN turno AS t ON t.id = cl.turno_id
LEFT JOIN aula AS a ON a.id = cl.aula_id
LEFT JOIN profesor AS prf ON prf.id_profesor = cl.profesor_id
LEFT JOIN hora_clase AS hc ON hc.id = cl.hora_clase_id
LEFT JOIN materias AS mat ON mat.id = cl.materias_id
LEFT JOIN cronograma_actividades AS cadt ON cadt.id = cl.cronograma_actividades_id
LEFT JOIN seccion AS secc ON secc.id = cadt.seccion_id
LEFT JOIN hora AS hclasi ON hclasi.id = hc.hora_ini_id
LEFT JOIN hora AS hclasf ON hclasf.id = hc.hora_fin_id
