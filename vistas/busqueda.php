<?php 
	include("../php/functiones.php");
?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Documento sin título</title>
		<link type="text/css" href="../jquery/css/smoothness/jquery-ui-1.9.0.custom.min.css" rel="stylesheet" />
		<link type="text/css" href="../jquery/css/demos.css" rel="stylesheet" />
		
		<script type="text/javascript" src="../jquery/js/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="../jquery/js/jquery-ui-1.9.0.custom.min.js"></script>
		<script type="text/javascript" src="../jquery/js/jquery.ui.datepicker-es.js"></script>
		
		<script type="text/javascript" src="../js/funciones2.js"></script>
		<script type="text/javascript">
			$(function(){
				$('#parametro').change(function(){
					cambiarclave();
				});
			});
		</script>
	</head>
	<body>
		<center>
			<div align="center"><h3> Búsqueda de Estudiantes</h3></div><br>
			<form action="" method="post" name="busqueda" id="busqueda">

				<select id="parametro" name="parametro"  title="Elija una clave de búsqueda. Ej. CEDULA">
					<option selected="selected" value="...">...</option>
					<option value="cedula" title="Para realizar búsqueda de un alumno específico.">Cedula</option>
					<option value="edad" title="Para realizar búsqueda de los alumnos que tengan una edad específica ejm 14" >Edad</option>
					<option value="periodo" title="Para buscar todos los alumnos de un periodo específico. Indique el periodo ejm 2012-2013">Periodo</option>        
					<option value="ano_curso" title="Para buscar todos los alumnos de un año específico. Indique el periodo y el año ejm 2012-2013 9(noveno)">A&ntilde;o en curso</option>
					<option value="seccion" title="Para buscar una sección especifica. Indique el año y la sección. ejm 1 A">Sección</option>
					<option value="sexo" title="Para realizar búsqueda por sexo. Escriba masculino o femenino">Sexo</option>
				</select>
				
				<select id="periodo" style="display:none;" name="periodo"  title="Se búscan a todos los alumnos del periodo que seleccione">
					<option value="" selected>...</option>
<?php

	$anyo = intval( Date("Y") );
	//Se coloca el período anterior a la fecha actual del servidor y los 3 períodos siguientes
	for($i=$anyo-0; $i<=$anyo+2; $i++){
?>
					<option value="<?php echo $i; ?>-<?php echo $i+1; ?>"><?php echo $i; ?>-<?php echo $i+1; ?></option>
<?php
	}

?>
				</select>


				<select id="ano_curso" style="display:none;" name="ano_curso"  title="Se realiza búsqueda de todos los alumnos en el año que seleccione.">
					<option selected="selected" value="">...</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>

				<select id="sexo" style="display:none;" name="sexo"  title="Elija si desea ver a los estudiantes de sexo femenino o masculino">
					<option selected="selected" value="">...</option>
					<option value="M">Masculino</option>
					<option value="F">Femenino</option>
				</select>


				<select id="seccion" style="display:none;" name="seccion"  title="Elija la sección a buscar">
					<option selected="selected" value="">...</option>
					<option value="a">A</option>
					<option value="b">B</option>
					
				</select>

				<input style="display:inline;" id="ci" onKeyPress="return IsNumber(event);" type="text" name="ci"  maxlength="8" title="Colocar el dato o información a buscar. Solo se aceptan NÚMEROS." />
				<input type="submit" name="enviar" value="Enviar" onClick="return validar()" /><br />
				<!--<center><font size="5" color="red"><b><b>Usuario Ya Registrado</b></b></font></center>-->
			</form>
		</center>
		<?php
			//valida y envía datos cédula o edad
			if(isset($_POST['enviar']) && isset($_POST['ci']) && $_POST['ci']!=""){ 
				echo buscar($_POST['ci'],false,$_POST['parametro'], false);
			}elseif(isset($_POST['enviar']) && isset($_POST['periodo']) && $_POST['periodo']!="" && isset($_POST['ano_curso']) && isset($_POST['seccion']) && $_POST['ano_curso']!="" && $_POST['seccion']!=""){
				echo buscar($_POST['seccion'],false,$_POST['parametro'], $_POST['ano_curso'],$_POST['periodo']);
			}elseif(isset($_POST['enviar']) && isset($_POST['periodo']) && $_POST['periodo']!="" && isset($_POST['ano_curso']) && $_POST['ano_curso']!=""){ 
				echo buscar(false,false,$_POST['parametro'],$_POST['ano_curso'],$_POST['periodo']);
			}elseif(isset($_POST['enviar']) && isset($_POST['sexo']) && $_POST['sexo']!=""){ 
				echo buscar($_POST['sexo'],false,$_POST['parametro'],false);
			}elseif(isset($_POST['enviar']) && isset($_POST['periodo']) && $_POST['periodo']!=""){ 
				echo buscar(false,false,$_POST['parametro'],false,$_POST['periodo']);
			}
		?>
	</body>
</html>
