<?php 
include("../php/functiones.php");
if(isset($_GET['cedula']) && empty($_GET['id2'])){
$arry=buscar($_GET['cedula'],'4');
}
	ob_start();
    $num = 'CMD01-'.date('ymd');
    $nom = 'DUPONT Alphonse';
    $date = '01/01/2012';
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	
?>
<html>
<head>
<style>
table{
	margin-left:20px;
	text-align:left;
	}
td{
	padding-top:20px;
	font-size:20;
	}
b{
	font-size:20px;
	text-transform:uppercase;
	}
p{
	margin-right:40px;
	text-align:right;
	}
</style>

<title>Formulario de Registro</title>

</head>

<body>
<table cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td width="747" height="150" align=left><img src="../img/insignia.png"align=left><br clear>
      <strong>REPUBLICA BOLIVARIANA DE VENEZUELA<br clear>
      MINISTERIO DEL PODER POPULAR PARA LA EDUCACION<br clear>
      Liceo Bolivariano "4 de febrero"<br clear>
    CARACAS</strong></td>
  </tr>
  <tr>
    <td align="center"><h1><u><em>PASE DE ENTRADA</em></u></h1>
      <br></td>
  </tr>
  <tr>
  	<td><p><em>Caracas<b>, <?php echo date('d'); ?></b> de <b><?php echo $meses[date('m')-1]; ?></b> del <b><?php echo date('Y'); ?>.</b></em></p></td>
		</tr>
  <tr>
    <td><em>Alumno (a): <b><u><?php echo $arry['nombre']." ".$arry['apellido']; ?></u></b> </em></td>
  </tr>
  <tr>
    <td><em>Titular de la c&eacute;dula de identidad <b>N&deg;: <u><?php echo $arry['cedula']; ?></u>,</b></em></td>
  </tr>
  <tr>
    <td><em>Cursa: <b><u><?php echo $arry['ano_curso']; ?>&deg;</u></b> Secci&oacute;n: <b><u><?php echo $arry['seccion']; ?></u>.</b></em></td>
  </tr>
  <tr>
    <td><em><strong>MOTIVO DEL RETARDO:__________________________________________</strong></em></td>
  </tr>
      <tr>
    <td><em>Conforme el pase. <strong>SE AUTORIZA</strong> la entrada a clase.</em></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
    <div align="center"><em><strong>Autorizado por:</strong></em></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr><td><div align="center"><b>_________________________</b></div></td></tr>
    <tr><td><div align="center"><em><strong>Coordinador de Seccional</strong></em></div></td></tr>
	<tr><td></td>
	</tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>



</body>
</html>

<?php
     $content = ob_get_clean();

    // convert
    require_once('../vistas/pdf/html2pdf/html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', 0);
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('ticket.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
	
?>




