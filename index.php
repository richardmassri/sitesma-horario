<?php
	session_start();
	if(isset($_SESSION['user']) and isset($_SESSION['clave']))
		header('Location: vistas/');
	
	include('php/functiones.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>..:: Sistema de Gestión Académica - Liceo Bolivariano 4 de Febrero ::..</title>
		<link href="css/estilos.css" rel="stylesheet" type="text/css" />
		<script>
			function validar ()
			{
				if (document.control.usuario.value=="")
				{
					alert ("No ha ingresado ningun Nombre");
					document.control.usuario.focus();
					return false;
				}
				else if (document.control.clave.value=="")
				{
					alert ("debe introducir una clave valida");
					document.control.clave.focus();
					return false;
				}
				else
					return true;
			}
		</script>
	</head>
	
	<body>
		<form id="control" name="control" method="post" action="" onsubmit="validar()"> 
			<table width="1046" border="0"  align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td height="92" colspan="4" align="center">
						<img src="./img/baneer_institucional.png" align="center" /><br>
						<img src="./img/baneer_principal.png" height="140px" align="center" />      
					</td>
				</tr> 
				<tr>
					<td height="35" colspan="4">
					<div align="right" style="font-family: sans-serif; font-size: 14px; font-weight: bold; color: #0B3A1B;">
					<b>Versión 2.0 </b><i>(Última Actualización: 20-09-2013)</i>
					</div>
					</td>
				</tr>				
				<tr>
					<td width="1080" height="280">
						<center>
							<div id="acceso" >
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td align="right">Usuario:</td>
										<td><input type="text" id="usuario" name="usuario" size="15"/></td>
									</tr>
									<tr>
										<td align="right">Clave:</td>
										<td><input type="password" id="clave" name="clave" size="15"/></td>
									</tr>
									<tr>
										<td colspan="2" align="center">
											<input type="submit" id="aceptar" name="aceptar" value="Aceptar" />
											<input id="cancelar" name="cancelar" type="reset" value="Cancelar" />
										</td>
									</tr>
								</table>
							</div>
						</center>
					</td>
				</tr>
				<tr>
					<td height="4" colspan="4" bgcolor="#0B3A1B"></td>
				</tr>
				
			</table>
		</form>
		<?php
			if(isset($_SESSION['user']) and isset($_SESSION['clave'])){
				unset($_SESSION['user']);
				unset($_SESSION['clave']);
				unset($_SESSION['perfil']);
			}
			if(isset($_POST['aceptar'])){
				$resul = validacion_usuario($_POST['usuario'],$_POST['clave']);
				if(array_key_exists('user', $resul)){
					$_SESSION['user'] = $resul['user'];
					$_SESSION['clave'] = $resul['clave'];
					$_SESSION['perfil'] = $resul['perfil'];	
					$_SESSION['id_usuario'] = $resul['id_usuario'];
					echo $resul['envio'];
				}
			}
		?>
	</body>
</html>

